﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repaso1
{
    class Program
    {
        public static int Menu()
        {
            int opcion;
            //int devuelve=9;

            Console.WriteLine("1º Inicializa la cadena a vacío");
            Console.WriteLine("2º Rellena una cadena con una palabra");
            Console.WriteLine("3º Escribir la cadena por pantalla");
            Console.WriteLine("4º Invertir la cadena");
            Console.WriteLine("5º Convertir la cadena a mayúsculas");
            Console.WriteLine("6º Convertir la cadena a minúsculas");
            Console.WriteLine("7º Rotar un carácter a la derecha");
            Console.WriteLine("8º Rotar un caracter a la izquierda");
            Console.WriteLine("9º Salir");


            /* CONVERSIONES
                cadena = numero.ToString()
                numero = int32.Parse(cadena);
             */
            //opcion = Console.ReadLine();
            opcion = Int32.Parse(Console.ReadLine());
            return opcion;
        }
        public static String VaciarCadena(String cadena)
        {
            return cadena = "";
        }

        public static String EscribirEnCadena(String cadena)
        {
            Console.WriteLine("Escribe la palabra: ");
            return Console.ReadLine();
        }
        public static void MostrarCadenaPantalla(String cadena)
        {
            Console.WriteLine(cadena);
        }
        public static String InvertirCadena(String cadena)
        {
            char[] letra = cadena.ToCharArray();
            Array.Reverse(letra);
            return new string(letra);
        }
        public static String ConvertirMayusculas(String cadena)
        {
            return cadena = cadena.ToUpper();
        }

        public static String ConvertirMinusculas(String cadena)
        {
            return cadena = cadena.ToLower();
        }

        public static String RotarDerecha(String cadena)
        {
            String temporal = cadena[0].ToString();
            cadena = cadena.Remove(0, 1);
            cadena += temporal;
            return cadena;
        }
        public static String RotarIzquierda(String cadena)
        {
            cadena = cadena.Substring(cadena.Length - 1) + cadena.Remove(cadena.Length - 1);
            return cadena;

        }
        public static void Main(string[] args)
        {
            int opcion;
            String cadena = "";

            do
            {
                opcion = Menu();
                switch (opcion)
                {
                    case 1:
                        cadena = VaciarCadena(cadena);
                        break;
                    case 2:
                        cadena = EscribirEnCadena(cadena);
                        break;
                    case 3:
                        MostrarCadenaPantalla(cadena);
                        break;
                    case 4:
                        cadena = InvertirCadena(cadena);
                        break;
                    case 5:
                        cadena = ConvertirMayusculas(cadena);
                        break;
                    case 6:
                        cadena = ConvertirMinusculas(cadena);
                        break;
                    case 7:
                        cadena = RotarDerecha(cadena);
                        break;
                    case 8:
                        cadena = RotarIzquierda(cadena);
                        break;
                }
            } while (opcion != 9);
        }
    }
}