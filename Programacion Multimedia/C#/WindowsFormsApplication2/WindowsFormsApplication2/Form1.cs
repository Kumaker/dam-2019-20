﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fraccion f1, f2, f3, f4;
            
            f1 = new Fraccion(12, 6);
            f2 = new Fraccion(3);
            f3 = new Fraccion(10, 10);
            f4 = new Fraccion(8, 10);

            f1.Multiplicar(f2);
            label1.Text = "f1 es: " + f1.Cadena() + "\n";
            f3.Dividir(f4);
            label1.Text += "f3 es :" + f3.Cadena() + "\n";
            f1.Simplificar();
            label1.Text += "f1 es: " + f1.Cadena() + "\n";
            f4.Elevar(Int32.Parse(textBox1.Text));
            label1.Text += "f4 es: " + f4.Cadena() + "\n";

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
