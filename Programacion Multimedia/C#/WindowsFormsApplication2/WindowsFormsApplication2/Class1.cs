﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2
{
    class Fraccion
    {
        private int numerador, denominador;
        public Fraccion(int n)
        {
            numerador = n;
            denominador = 1;
        }

        public Fraccion(int n, int d)
        {
            numerador = n;
            denominador = d;
        }

        public void Multiplicar(Fraccion f)
        {
            numerador = numerador * f.numerador;
            denominador = denominador * f.denominador;
        }

        public void Dividir(Fraccion f)
        {
            numerador   = numerador * f.denominador;
            denominador = denominador * f.numerador;
        }
        public void Simplificar()
        {
            bool encontrado = false;
            for (int i = 2; (i <= numerador) && (encontrado == false); i++)
            {
                if (((numerador % i) == 0) && ((denominador % i) == 0))
                {
                    numerador /= i;
                    denominador /= i;
                    encontrado = true;
                }
            }
        }
        public void Elevar(int e)
        {
            int vn=1, vd=1;
            for (int i = 1; i <= e; i++)
            {
                vn *= numerador;
                vd *= denominador;
            }
            numerador = vn;
            denominador = vd;
        }

        public string Cadena()
        {
            return numerador +" / "+ denominador;
        }
    }
}


