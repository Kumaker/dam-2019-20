﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplicationTipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;

            tipo = typeof(string);
            Console.WriteLine("El nombre corto es "
                + tipo.Name);
            Console.WriteLine("El nombre largo es "
                + tipo.FullName);
            Console.ReadKey();
        }
    }
}
