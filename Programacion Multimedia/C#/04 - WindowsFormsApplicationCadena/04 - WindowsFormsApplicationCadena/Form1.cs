﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04___WindowsFormsApplicationCadena
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void escribir_Click(object sender, EventArgs e)
        {
            resultado.Text = ""+ cadena.Text;
        }

        private void cadena_TextChanged(object sender, EventArgs e)
        {

        }

        private void inicializar_Click(object sender, EventArgs e)
        {
            cadena.Text = "";
        }

        private void invertir_Click(object sender, EventArgs e)
        {
            string cadena = resultado.Text; // cadena sera igual que el campo resultado, si fuese el campo texto solo se podria invertir la frase una vez
            char[] letra = cadena.ToCharArray(); // Aqui se transformara la cadena a caracteres
            Array.Reverse(letra); // Aqui revierte el orden de la cadena
            resultado.Text = new string(letra); // Finalmente el texto saldra por pantalla

        }

        private void rotarDerecha_Click(object sender, EventArgs e)
        {
            string cadena = resultado.Text;
            resultado.Text = cadena.Substring(cadena.Length - 1) + cadena.Remove(cadena.Length - 1); // Quita la ultima palabra para ponerla en primera
        }

        private void rotarIzquierda_Click(object sender, EventArgs e)
        {
            string cadena = resultado.Text;
            string temporal = cadena[0].ToString(); // temporal cogera la primera palabra
            cadena = cadena.Remove(0, 1); // Aqui la borramos
            cadena += temporal; // y aqui "sumamos" la palabra recogida
            resultado.Text = cadena;

        }

        private void mayusculas_Click(object sender, EventArgs e)
        {
            cadena.CharacterCasing = CharacterCasing.Upper; //  CharacterCasing.Upper es la funcion para hacer mayusculas las letras
            Clipboard.SetText(cadena.Text); // Esto copiara el texto del campo texto (donde escribire)
            resultado.Text = Clipboard.GetText(); // Copiara el texto copiado en el campo resultado para mostrarlo por pantalla

            // 

        }

        private void minusculas_Click(object sender, EventArgs e)
        {
            cadena.CharacterCasing = CharacterCasing.Lower; //  CharacterCasing.Lower es la funcion para hacer mayusculas las letras
            Clipboard.SetText(cadena.Text);
            resultado.Text = Clipboard.GetText();

        }
    }
}
