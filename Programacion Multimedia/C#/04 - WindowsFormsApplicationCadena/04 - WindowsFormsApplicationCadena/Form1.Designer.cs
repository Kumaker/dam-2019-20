﻿namespace _04___WindowsFormsApplicationCadena
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cadena = new System.Windows.Forms.TextBox();
            this.resultado = new System.Windows.Forms.Label();
            this.escribir = new System.Windows.Forms.Button();
            this.mayusculas = new System.Windows.Forms.Button();
            this.inicializar = new System.Windows.Forms.Button();
            this.invertir = new System.Windows.Forms.Button();
            this.rotarDerecha = new System.Windows.Forms.Button();
            this.rotarIzquierda = new System.Windows.Forms.Button();
            this.minusculas = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cadena
            // 
            this.cadena.Location = new System.Drawing.Point(12, 26);
            this.cadena.Name = "cadena";
            this.cadena.Size = new System.Drawing.Size(342, 20);
            this.cadena.TabIndex = 0;
            this.cadena.TextChanged += new System.EventHandler(this.cadena_TextChanged);
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.Location = new System.Drawing.Point(407, 33);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(75, 13);
            this.resultado.TabIndex = 1;
            this.resultado.Text = "La cadena es:";
            // 
            // escribir
            // 
            this.escribir.Location = new System.Drawing.Point(178, 87);
            this.escribir.Name = "escribir";
            this.escribir.Size = new System.Drawing.Size(126, 58);
            this.escribir.TabIndex = 2;
            this.escribir.Text = "Escribir";
            this.escribir.UseVisualStyleBackColor = true;
            this.escribir.Click += new System.EventHandler(this.escribir_Click);
            // 
            // mayusculas
            // 
            this.mayusculas.Location = new System.Drawing.Point(466, 87);
            this.mayusculas.Name = "mayusculas";
            this.mayusculas.Size = new System.Drawing.Size(124, 58);
            this.mayusculas.TabIndex = 3;
            this.mayusculas.Text = "Mayusculas";
            this.mayusculas.UseVisualStyleBackColor = true;
            this.mayusculas.Click += new System.EventHandler(this.mayusculas_Click);
            // 
            // inicializar
            // 
            this.inicializar.Location = new System.Drawing.Point(30, 87);
            this.inicializar.Name = "inicializar";
            this.inicializar.Size = new System.Drawing.Size(124, 58);
            this.inicializar.TabIndex = 4;
            this.inicializar.Text = "Inicializar";
            this.inicializar.UseVisualStyleBackColor = true;
            this.inicializar.Click += new System.EventHandler(this.inicializar_Click);
            // 
            // invertir
            // 
            this.invertir.Location = new System.Drawing.Point(321, 87);
            this.invertir.Name = "invertir";
            this.invertir.Size = new System.Drawing.Size(126, 58);
            this.invertir.TabIndex = 5;
            this.invertir.Text = "Invertir";
            this.invertir.UseVisualStyleBackColor = true;
            this.invertir.Click += new System.EventHandler(this.invertir_Click);
            // 
            // rotarDerecha
            // 
            this.rotarDerecha.Location = new System.Drawing.Point(178, 168);
            this.rotarDerecha.Name = "rotarDerecha";
            this.rotarDerecha.Size = new System.Drawing.Size(128, 61);
            this.rotarDerecha.TabIndex = 6;
            this.rotarDerecha.Text = "Rotar Derecha";
            this.rotarDerecha.UseVisualStyleBackColor = true;
            this.rotarDerecha.Click += new System.EventHandler(this.rotarDerecha_Click);
            // 
            // rotarIzquierda
            // 
            this.rotarIzquierda.Location = new System.Drawing.Point(321, 168);
            this.rotarIzquierda.Name = "rotarIzquierda";
            this.rotarIzquierda.Size = new System.Drawing.Size(126, 61);
            this.rotarIzquierda.TabIndex = 7;
            this.rotarIzquierda.Text = "Rotar Izquierda";
            this.rotarIzquierda.UseVisualStyleBackColor = true;
            this.rotarIzquierda.Click += new System.EventHandler(this.rotarIzquierda_Click);
            // 
            // minusculas
            // 
            this.minusculas.Location = new System.Drawing.Point(30, 171);
            this.minusculas.Name = "minusculas";
            this.minusculas.Size = new System.Drawing.Size(124, 58);
            this.minusculas.TabIndex = 8;
            this.minusculas.Text = "Minusculas";
            this.minusculas.UseVisualStyleBackColor = true;
            this.minusculas.Click += new System.EventHandler(this.minusculas_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 473);
            this.Controls.Add(this.minusculas);
            this.Controls.Add(this.rotarIzquierda);
            this.Controls.Add(this.rotarDerecha);
            this.Controls.Add(this.invertir);
            this.Controls.Add(this.inicializar);
            this.Controls.Add(this.mayusculas);
            this.Controls.Add(this.escribir);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.cadena);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox cadena;
        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.Button escribir;
        private System.Windows.Forms.Button mayusculas;
        private System.Windows.Forms.Button inicializar;
        private System.Windows.Forms.Button invertir;
        private System.Windows.Forms.Button rotarDerecha;
        private System.Windows.Forms.Button rotarIzquierda;
        private System.Windows.Forms.Button minusculas;
    }
}

