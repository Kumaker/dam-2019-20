﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurante
{
    public enum EstadoMesa { Libre, Reservada, Ocupada};
    class Mesas
    {
        // Atributos
        private int nOcupantesMax,
                    nOcupantesAct;
        private EstadoMesa estado; //Libre, Reservada, Ocupada
        private string HoraOcupacion;

        // Constructores
        public Mesas()
        {
            nOcupantesMax = 4;
            nOcupantesAct = 0;
            estado = EstadoMesa.Libre;
            HoraOcupacion = "";
        }

        public Mesas(int nOcMax, int nOcAct, EstadoMesa e, string horaO) { 

            nOcupantesMax = nOcMax;
            nOcupantesAct = nOcAct;
            estado = e;
            HoraOcupacion = horaO;
        }


        // OCUPAR
        public void Ocupar(int nOcupantes, string horaO)
        {
            if ((estado == EstadoMesa.Libre) && (nOcupantes <= nOcupantesMax))
            {
                estado = EstadoMesa.Ocupada;
                nOcupantesAct = nOcupantes;
                HoraOcupacion = horaO;
            }
        }
        // COBRAR
        public void Cobrar()
        {
            if (estado == EstadoMesa.Ocupada)
            {
                estado = EstadoMesa.Libre;
                HoraOcupacion = "";
            }

        }

        
    }
}
