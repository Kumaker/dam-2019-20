﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05___WindowsFormsApplicationClases
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCirculo1_Click(object sender, EventArgs e)
        {
            Circulo c1 = new Circulo(Int32.Parse(textBoxRadio1.Text),
                textBoxColor1.Text);
            double areaC1 = c1.Area();
            resultado.Text = "El area es " + areaC1;

        }
    }
}
