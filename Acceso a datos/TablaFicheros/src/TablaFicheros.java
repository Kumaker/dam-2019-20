import java.io.File;

public class TablaFicheros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] rutas = {"Nombre",
						  "Nombre\\bin",
						  "Nombre\\bin\\bytecode",
						  "Nombre\\src",
						  "Nombre\\src\\clases",
						  "Nombre\\doc",
						  "Nombre\\doc\\html",
						  "Nombre\\doc\\pdf"
						  };
		Boolean seguir = true;
		for (int i = 0; (i<rutas.length) && seguir; i++) {
			File carpeta = new File(rutas[i]);
			seguir = carpeta.mkdir();
		}
	}

}
