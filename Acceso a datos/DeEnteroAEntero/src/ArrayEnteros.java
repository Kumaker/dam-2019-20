import java.io.*;
import java.util.Scanner;

public class ArrayEnteros {

	public static int EscribirMenu() {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Menu de opciones");
		System.out.println("1. Leer fichero  en la tabla");
		System.out.println("2. Escribir tabla en el fichero");
		System.out.println("3. Escribir tabla en pantalla");
		System.out.println("4. Modificar tabla");
		System.out.println("5. Salir");
		
		return entrada.nextInt();
	}
	
	public static void EscribirTabla(int[] lista) {
		for (int i=0; i<lista.length; i++) {
			System.out.println(lista[i] + " ");
		}
	}
	
	public static void EscribirTablaFichero(String nombre, int[] lista) {
		String numeroCadena;
		try
		{
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			for(int i=0; i<lista.length; i++) {
				
			}
			fw.close();
		} catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}
	public static void LeerTabla(String nombre, int[] lista) {
		int resultado,
			numeroEntero,
			i;
		String numeroCadena;
		
		try 
		{
				File fichero = new File(nombre); 
				FileReader fr = new FileReader(fichero);
				
				i=0;
				numeroCadena = "";
				resultado = fr.read();
				while (resultado != -1) 
				{
					if (resultado == ' ') {
						numeroEntero = Integer.parseInt(numeroCadena);
						lista[i] = numeroEntero;
						i++;
						numeroCadena="";						
					} else {
						numeroCadena += (char)resultado;
						resultado = fr.read();
					}
				}
				fr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
