import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicherosFileWriter {

	public static void EscribirFichero(String nombre, String [] nombres){

		try 
		{
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);
			
			for(int i = 0; i<nombres.length; i++)
			{
				fw.write(nombres[i]);
				fw.write('\n');
			}
			//Cerrar el fichero
			fw.close();
			
			//Llamada a leer el fichero
			LeerFichero("FicheroTexto1.txt", nombres);
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	public static void LeerFichero(String nombre, String[] nombres) {
		try 
		{
			// 1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			//2. Lectura del fichero
			int letra;
			String cadena;
			int posicion = 0;
			
			cadena = "";
			letra = fr.read();
			while (letra != -1)
			{
				if (letra==';') {
					nombres[posicion] = cadena;
					cadena= "";
					posicion++;
				}else {
					//Tratamiento del caracter leido
					cadena += ((char) letra);
				}
				//Leer el siguiente caracter
				letra = fr.read();
			}
			
			//3. Cerrar el fichero
			fr.close();
			
			//4. Escribir la cade por pantalla
			System.out.println("He leido: " + cadena);
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
		}
		catch (IOException e) 
		{

			System.out.println(e.getMessage());
		}
	}
/*	public static void main(String[] args) {
		
		try {
			// 1. Apertura del fichero
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);
			
			// 2. Escribir en el fichero
			fw.write("Pepe");
			fw.write("Ana");
			fw.write("Juan");
			
			// 3. Cerrar fichero
			fw.close();
			
			//Llamada a leer el fichero
			LeerFichero("FicheroTexto1.txt");
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
		}

		
	}*/
	
	public static void InicializarTabla(String[] nombres)
	{
		for(int i=0;i<nombres.length;i++)
			nombres[i] = "";
	}
	public static void EscribirTabla(String[] nombres)
	{
		System.out.println("Contenido de la tabla: ");
		for(int i=0;i<nombres.length;i++)
			System.out.println(nombres[i] + " ");
	}
	public static void main(String[] args) {
	
		String[] listaNombres = {"Pepe", "Laura", "Juan", "Ana" };
		//Llamada a escribir fichero
		EscribirFichero("FicheroTexto1.txt", listaNombres);
		
		//Inicializar tabla
		InicializarTabla(listaNombres);
		EscribirTabla(listaNombres);
		
		//Llamada a leer fichero
		LeerFichero("FicheroTexto1.txt", listaNombres);
		EscribirTabla(listaNombres);
	
	}

}
