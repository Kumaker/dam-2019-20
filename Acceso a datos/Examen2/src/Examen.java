import java.io.*;
import java.util.*;

public class Examen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		File carpeta, // Carpeta creada
			 fichero; // Ficheros en la carpeta
		int  n1, n2;  // Numero introducidos
		Scanner leer = new Scanner(System.in);
		boolean resultado;
		
		System.out.println("Introduce el primer numero: ");
		n1 = leer.nextInt();
		System.out.println("Introduce el segundo numero: ");
		n2 = leer.nextInt();
		
		try {
			carpeta = new File(Integer.toString(n2));
			resultado = carpeta.mkdir();
			if (resultado == true) {
				for(int i=n1; i<=n2; i++) {
					fichero = new File(Integer.toString(n2) + "\\" +
									   Integer.toString(i) + ".num");
					fichero.createNewFile();
				}
			} 
		}
			catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
