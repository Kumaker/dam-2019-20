// ConsoleAplicationFicheroPersona.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include "stdafx.h"
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

/*ENUNCIADO*/
/* 1� Cargar datos            */
/* 2� Escribir datos          */
/* 3� Insertar tabla          */
/* 4� Escribir tabla pantalla */
/* 5� Borrar tabla            */
/* 6� Salir					  */

/*-------------------------------------
Declaracion de la estructura del alumno
---------------------------------------*/

struct TAlumno
{
	char nombre[40];
	int edad;
};

/*-------------------------------------
FUNCIONES
---------------------------------------*/

void cargarDatos(
	char nombreFichero[50],
	struct TAlumno tablaAlumnos[10],
	int *numero
	)
{
	FILE *pf;
	char nombreAlumno[40];
	int edadAlumno,
		posicion = 0;

	pf = fopen(nombreFichero, "rt");
	if (pf != NULL)
	{
		while (!feof(pf)) /*feof se utiliza para que mientras no sea el final del fichero, que siga leyendo*/
		{
			fscanf(pf, "%s", nombreAlumno);
			fscanf(pf, "%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno); /*no se puede asiganr un string sobre otro, hay que copiarlo por eso se usa strcpy*/
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		(*numero) = posicion;
		fclose(pf);
	}
}

void guardarDatos(
	char nombreFichero[50],
	struct TAlumno tablaAlumnos[10],
	int numero)
{
	FILE *pf;
	int posicion;

	pf = fopen(nombreFichero, "wt");
	if (pf != NULL)
	{
		for (posicion = 0; posicion < numero; posicion++) {
			fprintf(pf, " %s\n", tablaAlumnos[posicion].nombre);
			fprintf(pf, " %i\n", tablaAlumnos[posicion].edad);
		}
	}
	fclose(pf);
}

void imprimirTabla(
	struct TAlumno alumno[10])
{
	for (int cnt = 0;cnt < 11;cnt++){
		printf("Nombre: %s, Edad: %i \n", alumno[cnt].nombre, alumno[cnt].edad);
	}
}


void insertarDatos(
	struct TAlumno tablaAlumnos[10],
	int numero)
{


	for (int cnt = 0; cnt < 11; cnt++) {
		printf("dime un nombre: \n");
		scanf("%s", alumno.nombre);
		printf("dime la edad: \n");
		scanf("%i", &alumno.edad);
	}
}

void imprimirMenu() {
	
	printf("MENUD DE OPCIONES \n");
	printf("----------------- \n");
	printf(" 1. Cargar datos en la tabla \n");
	printf(" 2. Guardar datos en el fichero \n");
	printf(" 3. Escribir tabla por pantalla \n");
	printf(" 4. Insertar dato en tabla \n");
	printf(" 5. Borrar datos de la tabla \n");
	printf(" 6. Salir \n");
	printf("----------------- \n");
	printf("Introduce la opcion: ");

}
/* Funci�n punto de entrada */
int main(int argc, char *argv[]) {

	int opcion;
	struct TAlumno tablaAlumnos[10];
	int numeroAlumnos;

	numeroAlumnos = 0;
	
	do
	{
		imprimirMenu();
		scanf("%i", &opcion);
		switch (opcion)
		{
		case 1: cargarDatos("Clase.txt", tablaAlumnos, &numeroAlumnos);
			break;
		case 2: guardarDatos("Clase2.txt", tablaAlumnos, numeroAlumnos);
			break;
		case 3: imprimirTabla(tablaAlumnos); 
			break;
		}
	} while (opcion != 6);
	return EXIT_SUCCESS;
}
