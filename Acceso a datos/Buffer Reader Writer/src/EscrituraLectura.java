
public class EscrituraLectura {

	public static void MostrarPantalla(Persona[] tabla) {
		for(int i=0;i<tabla.length;i++) {
			System.out.println("Nombre: " + tabla[i].getNombre() + " Edad: " + tabla[i].getEdad());
		}
	}
	
	public static void VaciarTabla(Persona[] tabla) {
		for (int i=0; i<tabla.length;i++) {
			// Cambiando los valores del objeto
			tabla[i].setNombre("");
			tabla[i].setEdad(0);
			//Creando objetos nuevos
			tabla[i] = new Persona();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] miTabla = {"Juan", "Ana", "Pedro", "Laura"};
		String[] otraTabla = new String[10];
		
		FicheroEnBuffer miFichero = new FicheroEnBuffer("Nombre.txt");
		miFichero.EscribirTabla(miTabla);
		miFichero.LeerTabla(otraTabla);
		for(int i=0;i<otraTabla.length;i++) {
			System.out.println("Pos: " + i + "  " + otraTabla[i]);
		}
			// TABLA DE PERSONAS
			
			Persona[] misPersonas = new Persona[3];
			misPersonas[0] = new Persona("Juan", 19);
			misPersonas[1] = new Persona("Pedro Picapleitos", 22);
			misPersonas[2] = new Persona("Lorenzo", 32);
			
			MostrarPantalla(misPersonas);
			
			FicheroAlumnos miFicheroAl = new FicheroAlumnos("FAlumnos.txt");
			miFicheroAl.EscribirTabla(misPersonas);
			
			VaciarTabla(misPersonas);
			MostrarPantalla(misPersonas);
			miFicheroAl.EscribirTabla(misPersonas);
			MostrarPantalla(misPersonas);
			// Crear un metodo para sacar la informacion por pantalla
			// Escribir en un fichero
			// Leer el fichero
		}
	}
