import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroAlumnos {
	
	private String nombreFichero;
	
	public FicheroAlumnos(String n) {
		nombreFichero = n;
	}
	
	public void EscribirTabla(Persona[] tabla) {
		try {
			File fichero  = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0;i<tabla.length;i++) {
				bw.write(tabla[i].getNombre());  //Escribe un nombre por l�nea
				bw.newLine(); // un \n del propio BufferWrite
				bw.write(Integer.toString(tabla[i].getEdad())); //hay que convertir edad en String
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
