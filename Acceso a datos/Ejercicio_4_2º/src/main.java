import java.io.*;
import java.util.Scanner;

public class main {

	public static void contar(String frase) {
		char[] arrayFrase = new char[30];
		int cont = 0;
		arrayFrase = frase.toCharArray();
		
		for (int i = 0; i < arrayFrase.length; i++) {
			if      (arrayFrase[i] == 'a') 
				cont++;
			else if (arrayFrase[i] == 'A')
				 cont++;
			else if (arrayFrase[i] == 'e')
				cont++;
			else if (arrayFrase[i] == 'E')
				 cont++;
			else if (arrayFrase[i] == 'i')
				 cont++;
			else if (arrayFrase[i] == 'I')
				 cont++;
			else if (arrayFrase[i] == 'o')
				 cont++;
			else if (arrayFrase[i] == 'O')
				 cont++;
			else if (arrayFrase[i] == 'u')
				 cont++;
			else if (arrayFrase[i] == 'U')
				 cont++;
		}	
		
		System.out.println("Hay " + cont + " vocales");
		EscribirFichero(cont, frase);
	}
	public static void EscribirFichero(int vocales, String frase) {
		
		try {
			File fichero = new File("vocales.txt");
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write("La frase: " + frase);
			bw.newLine();
			bw.write("Tiene " + vocales + " vocales");	
			bw.newLine();
			
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("COMPROVADOR DE VOCALES");
		System.out.println("Buenos dias, por favor introduzca la frase a analizar");
		System.out.print(": ");
		String frase = scanner.nextLine();
		contar(frase);	
	}
}
