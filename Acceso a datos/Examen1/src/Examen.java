import java.io.*;

public class Examen {
	
	public static void LeerFichero(String nombre, String [] tNombres, int[] tDni) {
		int resultado, 		// Variable para leer
			i,				// Indice tablas
			nEmpleados;		// N� Empleados del fichero
		String valorCadena; // Convertir los datos leidos
		boolean numero, 	// Indica si lee numero en el fichero
				primero;	//Indica si se lee el primer numero del fichero
		
		try 
		{
			File  fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			numero = true;
			primero = true;
			i=0;
			valorCadena = "";
			resultado = fr.read();
			while ( resultado != -1) {
				if (resultado != '-') {
					valorCadena += (char)resultado;
				} else { // No para hasta que encuentra un gui�n
					if (numero==true) {
						if(primero==true) {
							nEmpleados = Integer.parseInt(valorCadena);
							primero = false;
						}
						else {
							tDni[i] = Integer.parseInt(valorCadena);
							i++; // i++ va aqui porque el dni es lo ultimo que se mete y cuando metemos nombre no tenemos que incrementar la i 
						}
						numero = false;
					} else { //numero = false --> Leemos un nombre
						tNombres[i] = valorCadena;
						numero = true;
					}
					valorCadena = ""; 
					}
				resultado = fr.read();
			}
		fr.close();
		}
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
		}
		catch (IOException e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static void EscribirPantalla(String[] tNombres, int[] tDNIs) {
		System.out.println("El contenido es: ");
		for(int i=0;i<tNombres.length; i++) {
			System.out.println(tNombres[i] + " - " + tDNIs[i]);
		}
	}
	
	public static void EscribirMayorDNI(String[] tNombres, int[] TDNIs) {
		int DNIMaximo, posMax;
		
		DNIMaximo = -100000;
		posMax = 0;
		for (int i=0; i<TDNIs.length; i++)
		{
			if (TDNIs[i] > DNIMaximo) {
				DNIMaximo = TDNIs[i];
				posMax = i;
			}
		}
		System.out.println("El nombre del DNI mayor es " + tNombres[posMax]);
	}
	
	public static void EscribirMenorDNI(String[] tNombres, int[] TDNIs) {
		int DNIMinimo, posMax;
		
		DNIMinimo = 10000000;
		posMax = 0;
		
		for (int i = 0; i<TDNIs.length;i++) {
			if((TDNIs[i] != 0) && (TDNIs[i] < DNIMinimo)){
				DNIMinimo = TDNIs[i];
				posMax = i;
			}
		}
		System.out.println("El nombre del DNI menor es " + tNombres[posMax]);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] tablaNombres = new String[10];
		int[] 	 tablaDnis 	  = new int[10];
		
		LeerFichero("Empleados.txt", tablaNombres,tablaDnis);
		EscribirPantalla(tablaNombres, tablaDnis);
		EscribirMayorDNI(tablaNombres, tablaDnis);
		EscribirMenorDNI(tablaNombres, tablaDnis);
	}

}
