
public class Articulo {

	private String nombre;
	private int precio;
	
	public Articulo(String name, int cost) {
		
		nombre = name;
		precio = cost;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}
}
