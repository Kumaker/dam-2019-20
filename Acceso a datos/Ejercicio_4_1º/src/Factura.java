
public class Factura {
	
	private int id, cantidad;
	
	public Factura(int bill, int article) {
		
		id = bill;
		cantidad = article;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
}
