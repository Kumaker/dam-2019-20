import java.io.*;

public class FicherosFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		File carpeta = new File("CARPETA");
		
		if (carpeta.mkdir() == true ) {
			System.out.println("Carperta creada");
			
			File fich1 = new File(carpeta, "primero.txt"),
			     fich2 = new File(carpeta, "segundo.txt");
			
			try {
				fich1.createNewFile();
				fich2.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		} else {
			System.out.println("Error al crear la carpeta");
		}
		
	}

}
