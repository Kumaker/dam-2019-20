import java.io.*;

public class Main {

	public static void LeerCategoria(String fichero, Categoria ListaCat[]) {
		String Linea;
		String Nombre = "";
		int Id;
		int i = 0;
		
		try {
			File file = new File(fichero);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			Linea = br.readLine();
			while(Linea != null) {
				Id = Integer.parseInt(Linea);
				Nombre = br.readLine();
				ListaCat[i] = new Categoria(Id, Nombre);
				i++;
				Linea = br.readLine();
			} 
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public static void LeerProducto(String fichero, Producto ListaPro[]) {
		String Linea;
		String Nombre;
		int IdPro;
		int IdCat;
		int i = 0;
		
		try {
			File file = new File(fichero);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			
			Linea = br.readLine();
			while(Linea != null) {
				IdPro = Integer.parseInt(Linea);
				Nombre = br.readLine();
				IdCat = Integer.parseInt(br.readLine());
				ListaPro[i] = new Producto(IdPro, Nombre, IdCat);
				i++;
				Linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Categoria tablaCat[] = new Categoria[4];
		Producto tablaPro[] = new Producto[5];
		String nombreCat;
		int idCat ,idPro;
		int np;
		
		LeerCategoria("FicheroCategoria.txt", tablaCat);
		LeerProducto("FicheroProducto.txt", tablaPro);
		
		for (int i = 0; i<tablaCat.length; i++) {
			np = 0;
			nombreCat = tablaCat[i].getNombre();
			for (int p = 0; p<tablaPro.length; p++) {
					if(tablaCat[i].getId() == tablaPro[p].getIdCat())
						np++;
			}
			System.out.println("la categoria: " + nombreCat + "tiene: " + np);
		}
		
	}

}
