
public class Categoria {

	private int IdCategoria;
	private String Nombre;
	
		public Categoria( int idCategoria, String n) {
			IdCategoria = idCategoria;
			Nombre = n;
		}
		
		public int getId() {
			return IdCategoria;
		}
		public String getNombre() {
			return Nombre;
		}
}
