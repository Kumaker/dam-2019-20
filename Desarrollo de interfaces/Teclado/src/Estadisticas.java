import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class Estadisticas {

	public String nombre;
	private JFrame FrameEsta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Estadisticas window = new Estadisticas();
					window.FrameEsta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void setNombre(String n) {
		nombre = n;
	}

	/**
	 * Create the application.
	 */
	public Estadisticas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		FrameEsta = new JFrame();
		FrameEsta.setTitle("Mecano");
		FrameEsta.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
		FrameEsta.setResizable(false);
		FrameEsta.setBounds(100, 100, 571, 432);
		FrameEsta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrameEsta.getContentPane().setLayout(null);
		
		JButton Estadisticas = new JButton("Estadisticas");
		Estadisticas.setBounds(145, 50, 226, 113);
		FrameEsta.getContentPane().add(Estadisticas);
		
		JButton Jugar = new JButton("Jugar");
		Jugar.setBounds(145, 229, 226, 113);
		FrameEsta.getContentPane().add(Jugar);
		
		//EVENTOS
		
		Estadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					LectorFicheros lector = new LectorFicheros();
					String texto = lector.contenidoArchivo("archivos//Ranking" + nombre + ".txt");
					Ranking rank = new Ranking(texto);
					rank.getFrame().setVisible(true);
				} catch (IOException e2) { 
					System.out.println(e2.getMessage());
				}
			
				
			}
		});
		Jugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Mecanografia mecanografia = new Mecanografia();
				mecanografia.getFrameMeca().setVisible(true);
				mecanografia.setNombre(nombre);
				FrameEsta.dispose();
			}
		});
	}

	public JFrame getFrameEsta() {
		return FrameEsta;
	}

	public void setFrameEsta(JFrame frameEsta) {
		FrameEsta = frameEsta;
	}
}
