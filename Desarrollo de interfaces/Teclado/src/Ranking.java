import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ranking {
 
	public String texto;
	public JFrame getFrame() {
		return frmMecano;
	}

	public void setFrame(JFrame frame) {
		this.frmMecano = frame;
	}

	private JFrame frmMecano;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ranking window = new Ranking("Prueba");
					window.frmMecano.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ranking(String Texto) {
		this.texto = Texto;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecano = new JFrame();
		frmMecano.setTitle("Mecano");
		frmMecano.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
		frmMecano.setResizable(false);
		frmMecano.setBounds(100, 100, 563, 411);
		frmMecano.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecano.getContentPane().setLayout(null);
		
		JTextPane TextPane = new JTextPane();
		TextPane.setBounds(0, 0, 557, 299);
		frmMecano.getContentPane().add(TextPane);
		TextPane.setText(texto);
		
		JButton BtnVolver = new JButton("Volver");
		BtnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMecano.dispose();
			}
		});
		BtnVolver.setBounds(210, 310, 130, 39);
		frmMecano.getContentPane().add(BtnVolver);
		
		
	}
}
