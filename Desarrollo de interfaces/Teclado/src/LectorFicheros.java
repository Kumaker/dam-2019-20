import java.io.*;


public class LectorFicheros {

	public static String contenidoArchivo(String file) throws FileNotFoundException, IOException {
		String cadena = "",
			    texto = "";
		FileReader read = new FileReader(file);
		BufferedReader buffer = new BufferedReader(read);
		while((cadena = buffer.readLine())!=null) {
			texto += cadena + '\n';
		}
		buffer.close();
		
		return texto;
	}


public boolean find(String file, String pass, String user) throws FileNotFoundException {
	
	String linea="";
	boolean login = false;
	
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(file));
			
			while ((linea = buffer.readLine())!=null && login == false) {
				if(linea.equals(user + " " + pass))
					login = true;
				
				else if (!login)
					login = false;
			}
			buffer.close();
		} catch (IOException e) {
			System.out.println("Error en el archivo");
		}
			return login;
	}
public static void EscribirFichero(String nombre, String file, int cont, int errores, int aciertos) {
	try {
		File fichero = new File(file);
		FileWriter writer = new FileWriter(fichero);
		BufferedWriter buffer = new BufferedWriter(writer);
		buffer.newLine();
		buffer.write("Usuario : " + nombre);
		buffer.newLine();
		buffer.write("Pulsaciones : " + cont);
		buffer.newLine();
		buffer.write("Aciertos: " + aciertos);
		buffer.newLine();
		buffer.write("Errores: " + errores);
		buffer.close();
		
		} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
}
public static boolean ComprobarLetra(char letra, char[] texto, int contador) {
	boolean resultado = false;
	
	if(Character.isAlphabetic(letra) || letra == ' ') {
		if (letra == texto[contador]) {
			resultado = true;
		}
	}
	return resultado;

	
}

}
