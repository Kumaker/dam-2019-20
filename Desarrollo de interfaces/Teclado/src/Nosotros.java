import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Toolkit;

public class Nosotros {

	private static JFrame frmNosotros;
	private JTextField txtMiguelALopez;
	private JTextField txtMiguelangellopezgmailcom;
	private JTextField txtDam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Nosotros window = new Nosotros();
					window.frmNosotros.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Nosotros() {
		initialize();
	}
	public static JFrame getFrame() {
		return frmNosotros;
	}

	public void setFrame(JFrame frame) {
		this.frmNosotros = frame;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNosotros = new JFrame();
		frmNosotros.setTitle("Mecano");
		frmNosotros.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
		frmNosotros.setBounds(100, 100, 327, 240);
		frmNosotros.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNosotros.getContentPane().setLayout(null);
		
		JLabel lblCreador = new JLabel("Creador");
		lblCreador.setBounds(28, 33, 80, 26);
		frmNosotros.getContentPane().add(lblCreador);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(28, 84, 80, 26);
		frmNosotros.getContentPane().add(lblEmail);
		
		JLabel lblClase = new JLabel("Clase");
		lblClase.setBounds(28, 138, 80, 26);
		frmNosotros.getContentPane().add(lblClase);
		
		txtMiguelALopez = new JTextField();
		txtMiguelALopez.setEnabled(false);
		txtMiguelALopez.setEditable(false);
		txtMiguelALopez.setText("Miguel A. lopez Hernandez");
		txtMiguelALopez.setBounds(85, 36, 163, 20);
		frmNosotros.getContentPane().add(txtMiguelALopez);
		txtMiguelALopez.setColumns(10);
		
		txtMiguelangellopezgmailcom = new JTextField();
		txtMiguelangellopezgmailcom.setEnabled(false);
		txtMiguelangellopezgmailcom.setEditable(false);
		txtMiguelangellopezgmailcom.setText("miguelangellopez1307@gmail.com");
		txtMiguelangellopezgmailcom.setColumns(10);
		txtMiguelangellopezgmailcom.setBounds(85, 87, 216, 20);
		frmNosotros.getContentPane().add(txtMiguelangellopezgmailcom);
		
		txtDam = new JTextField();
		txtDam.setEnabled(false);
		txtDam.setEditable(false);
		txtDam.setText("4\u00BA DAM");
		txtDam.setColumns(10);
		txtDam.setBounds(85, 141, 51, 20);
		frmNosotros.getContentPane().add(txtDam);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(197, 167, 89, 23);
		frmNosotros.getContentPane().add(btnVolver);
		
		//EVENTOS
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/*Login login = new Login();
				login.getFrmLogin().setVisible(true);*/
				frmNosotros.dispose();
			}
		});
	
	}
}
