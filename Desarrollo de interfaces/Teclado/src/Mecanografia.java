import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.Timer;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.JTextArea;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.Toolkit;

public class Mecanografia {
	public char[] ArrayTexto;
	public int aciertos = 0, error = 0;
	private JFrame frameMeca;
	public String nombre;
	JTextArea tiempo;
	Timer timer;
	int count;
	static int cnt = 0;
	
	static DefaultHighlighter.DefaultHighlightPainter highlightResaltarCorrecto = new DefaultHighlighter.DefaultHighlightPainter(
			Color.green);
	static DefaultHighlighter.DefaultHighlightPainter highlightResaltarIncorrecto = new DefaultHighlighter.DefaultHighlightPainter(
			Color.red);

	public static void correcto(JTextArea Colorear) {
		try {
			Colorear.getHighlighter().addHighlight(cnt, cnt + 1, highlightResaltarCorrecto);
			cnt++;
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	public void setNombre(String n) {
		nombre = n;
	}

	public static void incorrecto(JTextArea Colorear) {

		try {
			Colorear.getHighlighter().addHighlight(cnt, cnt + 1, highlightResaltarIncorrecto);
			cnt++;
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	
	public void IniciaTimer() {
		timer = new Timer (1000, new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				count++;
				 if (count <= 500) {
					 tiempo.setText("Tiempo: " + Integer.toString(count));
				 } else {
					 timer.stop();
				 }
			}
		}); 
		timer.start();
	}
	private int contador = 0;
	private JTextArea pulsaciones, errores, acertado, text;
	
	public void contador(char letra) {
		boolean resultado = LectorFicheros.ComprobarLetra(letra, ArrayTexto, contador);
		if(Character.isAlphabetic(letra) || letra == ' ') {
			contador++;
			if (resultado) {
				buscarTecla(letra).setBackground(Color.green);
				aciertos++;
				acertado.setText("Aciertos: " + aciertos);
				correcto(text);
			} else {
				buscarTecla(letra).setBackground(Color.red);
				error++;
				errores.setText("Errores: " + error);
				incorrecto(text);
			}
		}
		pulsaciones.setText("Pulsaciones: " + getContador());
		
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
		
		
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mecanografia window = new Mecanografia();
					window.frameMeca.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mecanografia() {
		initialize();
	}

	public JFrame getFrameMeca() {
		return frameMeca;
	}

	public void setFrameMeca(JFrame frameMeca) {
		this.frameMeca = frameMeca;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameMeca = new JFrame();
		frameMeca.setTitle("Mecano");
		frameMeca.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
		frameMeca.setAlwaysOnTop(true);
		frameMeca.setExtendedState(JFrame.MAXIMIZED_BOTH);
		//frameMeca.setResizable(false);
		//frameMeca.setBounds(100, 100, 789, 539);
		frameMeca.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMeca.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(3, 16, 0, 0);
		frameMeca.getContentPane().add(label);
		
		pulsaciones = new JTextArea();
		pulsaciones.setBounds(48, 5, 121, 27);
		pulsaciones.setText("Pulsaciones: " + getContador());
		pulsaciones.setEnabled(false);
		frameMeca.getContentPane().add(pulsaciones);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(129, 16, 0, 0);
		frameMeca.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(134, 16, 0, 0);
		frameMeca.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(139, 16, 0, 0);
		frameMeca.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("");
		label_4.setBounds(144, 16, 0, 0);
		frameMeca.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("");
		label_5.setBounds(149, 16, 0, 0);
		frameMeca.getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("");
		label_6.setBounds(154, 16, 0, 0);
		frameMeca.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("");
		label_7.setBounds(159, 16, 0, 0);
		frameMeca.getContentPane().add(label_7);
		
		JLabel label_8 = new JLabel("");
		label_8.setBounds(164, 16, 0, 0);
		frameMeca.getContentPane().add(label_8);
		
		errores = new JTextArea();
		errores.setBounds(272, 5, 106, 27);
		errores.setText("Errores: ");
		errores.setEnabled(false);
		frameMeca.getContentPane().add(errores);
		
		JLabel label_9 = new JLabel("");
		label_9.setBounds(250, 16, 0, 0);
		frameMeca.getContentPane().add(label_9);
		
		JLabel label_10 = new JLabel("");
		label_10.setBounds(255, 16, 0, 0);
		frameMeca.getContentPane().add(label_10);
		
		JLabel label_11 = new JLabel("");
		label_11.setBounds(260, 16, 0, 0);
		frameMeca.getContentPane().add(label_11);
		
		JLabel label_12 = new JLabel("");
		label_12.setBounds(265, 16, 0, 0);
		frameMeca.getContentPane().add(label_12);
		
		JLabel label_13 = new JLabel("");
		label_13.setBounds(270, 16, 0, 0);
		frameMeca.getContentPane().add(label_13);
		
		tiempo = new JTextArea();
		tiempo.setBounds(605, 5, 96, 27);
		tiempo.setText("Tiempo: ");
		tiempo.setEnabled(false);
		frameMeca.getContentPane().add(tiempo);
		
		JLabel label_14 = new JLabel("");
		label_14.setBounds(348, 16, 0, 0);
		frameMeca.getContentPane().add(label_14);
		
		JLabel label_15 = new JLabel("");
		label_15.setBounds(353, 16, 0, 0);
		frameMeca.getContentPane().add(label_15);
		
		JLabel label_16 = new JLabel("");
		label_16.setBounds(358, 16, 0, 0);
		frameMeca.getContentPane().add(label_16);
		
		JLabel label_17 = new JLabel("");
		label_17.setBounds(363, 16, 0, 0);
		frameMeca.getContentPane().add(label_17);
		
		JLabel label_18 = new JLabel("");
		label_18.setBounds(368, 16, 0, 0);
		frameMeca.getContentPane().add(label_18);
		
		JLabel label_19 = new JLabel("");
		label_19.setBounds(373, 16, 0, 0);
		frameMeca.getContentPane().add(label_19);
		
		JButton primeralec = new JButton("1\u00BA Leccion");
		primeralec.setBounds(18, 44, 151, 55);
		frameMeca.getContentPane().add(primeralec);
		
		JLabel label_20 = new JLabel("");
		label_20.setBounds(464, 16, 0, 0);
		frameMeca.getContentPane().add(label_20);
		
		JLabel label_21 = new JLabel("");
		label_21.setBounds(480, 16, 0, 0);
		frameMeca.getContentPane().add(label_21);
		
		JLabel label_22 = new JLabel("");
		label_22.setBounds(485, 16, 0, 0);
		frameMeca.getContentPane().add(label_22);
		
		JLabel label_23 = new JLabel("");
		label_23.setBounds(490, 16, 0, 0);
		frameMeca.getContentPane().add(label_23);
		
		JLabel label_24 = new JLabel("");
		label_24.setBounds(495, 16, 0, 0);
		frameMeca.getContentPane().add(label_24);
		
		JLabel label_25 = new JLabel("");
		label_25.setBounds(500, 16, 0, 0);
		frameMeca.getContentPane().add(label_25);
		
		JLabel label_26 = new JLabel("");
		label_26.setBounds(505, 16, 0, 0);
		frameMeca.getContentPane().add(label_26);
		
		JLabel label_27 = new JLabel("");
		label_27.setBounds(510, 16, 0, 0);
		frameMeca.getContentPane().add(label_27);
		
		JLabel label_28 = new JLabel("");
		label_28.setBounds(515, 16, 0, 0);
		frameMeca.getContentPane().add(label_28);
		
		JLabel label_29 = new JLabel("");
		label_29.setBounds(520, 16, 0, 0);
		frameMeca.getContentPane().add(label_29);
		
		JLabel label_30 = new JLabel("");
		label_30.setBounds(525, 16, 0, 0);
		frameMeca.getContentPane().add(label_30);
		
		JLabel label_31 = new JLabel("");
		label_31.setBounds(530, 16, 0, 0);
		frameMeca.getContentPane().add(label_31);
		
		JLabel label_32 = new JLabel("");
		label_32.setBounds(535, 16, 0, 0);
		frameMeca.getContentPane().add(label_32);
		
		JLabel label_33 = new JLabel("");
		label_33.setBounds(540, 16, 0, 0);
		frameMeca.getContentPane().add(label_33);
		
		JLabel label_34 = new JLabel("");
		label_34.setBounds(545, 16, 0, 0);
		frameMeca.getContentPane().add(label_34);
		
		JLabel label_35 = new JLabel("");
		label_35.setBounds(550, 16, 0, 0);
		frameMeca.getContentPane().add(label_35);
		
		JLabel label_36 = new JLabel("");
		label_36.setBounds(555, 16, 0, 0);
		frameMeca.getContentPane().add(label_36);
		
		JLabel label_37 = new JLabel("");
		label_37.setBounds(560, 16, 0, 0);
		frameMeca.getContentPane().add(label_37);
		
		JLabel label_38 = new JLabel("");
		label_38.setBounds(565, 16, 0, 0);
		frameMeca.getContentPane().add(label_38);
		
		JLabel label_39 = new JLabel("");
		label_39.setBounds(570, 16, 0, 0);
		frameMeca.getContentPane().add(label_39);
		
		JLabel label_40 = new JLabel("");
		label_40.setBounds(575, 16, 0, 0);
		frameMeca.getContentPane().add(label_40);
		
		JButton segundalec = new JButton("2\u00BA Leccion");
		segundalec.setBounds(18, 145, 151, 55);
		frameMeca.getContentPane().add(segundalec);
		
		JLabel label_41 = new JLabel("");
		label_41.setBounds(666, 16, 0, 0);
		frameMeca.getContentPane().add(label_41);
		
		JLabel label_42 = new JLabel("");
		label_42.setBounds(671, 16, 0, 0);
		frameMeca.getContentPane().add(label_42);
		
		JLabel label_43 = new JLabel("");
		label_43.setBounds(676, 16, 0, 0);
		frameMeca.getContentPane().add(label_43);
		
		JLabel label_44 = new JLabel("");
		label_44.setBounds(681, 16, 0, 0);
		frameMeca.getContentPane().add(label_44);
		
		JLabel label_45 = new JLabel("");
		label_45.setBounds(686, 16, 0, 0);
		frameMeca.getContentPane().add(label_45);
		
		JLabel label_46 = new JLabel("");
		label_46.setBounds(691, 16, 0, 0);
		frameMeca.getContentPane().add(label_46);
		
		JLabel label_47 = new JLabel("");
		label_47.setBounds(696, 16, 0, 0);
		frameMeca.getContentPane().add(label_47);
		
		JLabel label_48 = new JLabel("");
		label_48.setBounds(701, 16, 0, 0);
		frameMeca.getContentPane().add(label_48);
		
		JLabel label_49 = new JLabel("");
		label_49.setBounds(706, 16, 0, 0);
		frameMeca.getContentPane().add(label_49);
		
		JLabel label_50 = new JLabel("");
		label_50.setBounds(711, 16, 0, 0);
		frameMeca.getContentPane().add(label_50);
		
		JLabel label_51 = new JLabel("");
		label_51.setBounds(716, 16, 0, 0);
		frameMeca.getContentPane().add(label_51);
		
		JLabel label_52 = new JLabel("");
		label_52.setBounds(721, 16, 0, 0);
		frameMeca.getContentPane().add(label_52);
		
		JLabel label_53 = new JLabel("");
		label_53.setBounds(726, 16, 0, 0);
		frameMeca.getContentPane().add(label_53);
		
		JLabel label_54 = new JLabel("");
		label_54.setBounds(731, 16, 0, 0);
		frameMeca.getContentPane().add(label_54);
		
		JLabel label_55 = new JLabel("");
		label_55.setBounds(736, 16, 0, 0);
		frameMeca.getContentPane().add(label_55);
		
		JLabel label_56 = new JLabel("");
		label_56.setBounds(741, 16, 0, 0);
		frameMeca.getContentPane().add(label_56);
		
		JLabel label_57 = new JLabel("");
		label_57.setBounds(746, 16, 0, 0);
		frameMeca.getContentPane().add(label_57);
		
		JLabel label_58 = new JLabel("");
		label_58.setBounds(751, 16, 0, 0);
		frameMeca.getContentPane().add(label_58);
		
		JLabel label_59 = new JLabel("");
		label_59.setBounds(756, 16, 0, 0);
		frameMeca.getContentPane().add(label_59);
		
		JLabel label_60 = new JLabel("");
		label_60.setBounds(761, 16, 0, 0);
		frameMeca.getContentPane().add(label_60);
		
		JLabel label_61 = new JLabel("");
		label_61.setBounds(766, 16, 0, 0);
		frameMeca.getContentPane().add(label_61);
		
		JLabel label_62 = new JLabel("");
		label_62.setBounds(771, 16, 0, 0);
		frameMeca.getContentPane().add(label_62);
		
		
		JButton btnQ = new JButton("Q");
		btnQ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnQ.setBounds(267, 503, 55, 55);
		btnQ.setEnabled(false);
		frameMeca.getContentPane().add(btnQ);
		btnQ.setName("btnQ");
		
		JLabel label_63 = new JLabel("");
		label_63.setBounds(822, 16, 0, 0);
		frameMeca.getContentPane().add(label_63);
		
		JLabel label_64 = new JLabel("");
		label_64.setBounds(827, 16, 0, 0);
		frameMeca.getContentPane().add(label_64);
		
		JButton btnW = new JButton("W");
		btnW.setBounds(332, 503, 55, 55);
		btnW.setEnabled(false);
		frameMeca.getContentPane().add(btnW);
		btnW.setName("btnW");
		
		JLabel label_65 = new JLabel("");
		label_65.setBounds(880, 16, 0, 0);
		frameMeca.getContentPane().add(label_65);
		
		JButton btnE = new JButton("E");
		btnE.setBounds(397, 503, 55, 55);
		btnE.setEnabled(false);
		frameMeca.getContentPane().add(btnE);
		btnE.setName("btnE");
		
		JLabel label_66 = new JLabel("");
		label_66.setBounds(929, 16, 0, 0);
		frameMeca.getContentPane().add(label_66);
		
		JButton btnR = new JButton("R");
		btnR.setBounds(464, 503, 55, 55);
		btnR.setEnabled(false);
		frameMeca.getContentPane().add(btnR);
		btnR.setName("btnR");
		
		JLabel label_67 = new JLabel("");
		label_67.setBounds(978, 16, 0, 0);
		frameMeca.getContentPane().add(label_67);
		
		JLabel label_68 = new JLabel("");
		label_68.setBounds(983, 16, 0, 0);
		frameMeca.getContentPane().add(label_68);
		
		JButton btnT = new JButton("T");
		btnT.setBounds(530, 503, 55, 55);
		btnT.setEnabled(false);
		frameMeca.getContentPane().add(btnT);
		btnT.setName("btnT");
		
		JLabel label_69 = new JLabel("");
		label_69.setBounds(1032, 16, 0, 0);
		frameMeca.getContentPane().add(label_69);
		
		JButton btnY = new JButton("Y");
		btnY.setBounds(595, 503, 55, 55);
		btnY.setEnabled(false);
		frameMeca.getContentPane().add(btnY);
		btnY.setName("btnY");
		
		JLabel label_70 = new JLabel("");
		label_70.setBounds(8, 44, 0, 0);
		frameMeca.getContentPane().add(label_70);
		
		JButton btnU = new JButton("U");
		btnU.setBounds(666, 503, 55, 55);
		btnU.setEnabled(false);
		frameMeca.getContentPane().add(btnU);
		btnU.setName("btnU");
		
		JLabel label_71 = new JLabel("");
		label_71.setBounds(57, 44, 0, 0);
		frameMeca.getContentPane().add(label_71);
		
		JButton btnI = new JButton("I");
		btnI.setBounds(736, 503, 55, 55);
		btnI.setEnabled(false);
		frameMeca.getContentPane().add(btnI);
		btnI.setName("btnI");
		
		JLabel label_72 = new JLabel("");
		label_72.setBounds(104, 44, 0, 0);
		frameMeca.getContentPane().add(label_72);
		
		JButton btnO = new JButton("O");
		btnO.setBounds(801, 503, 55, 55);
		btnO.setEnabled(false);
		frameMeca.getContentPane().add(btnO);
		btnO.setName("btnO");
		
		JLabel label_73 = new JLabel("");
		label_73.setBounds(155, 44, 0, 0);
		frameMeca.getContentPane().add(label_73);
		
		JButton btnP = new JButton("P");
		btnP.setBounds(866, 503, 55, 55);
		btnP.setEnabled(false);
		frameMeca.getContentPane().add(btnP);
		btnP.setName("btnP");
		
		JLabel label_74 = new JLabel("");
		label_74.setBounds(204, 44, 0, 0);
		frameMeca.getContentPane().add(label_74);
		
		JLabel label_75 = new JLabel("");
		label_75.setBounds(209, 44, 0, 0);
		frameMeca.getContentPane().add(label_75);
		
		JButton btnA = new JButton("A");
		btnA.setBounds(308, 569, 55, 55);
		btnA.setEnabled(false);
		frameMeca.getContentPane().add(btnA);
		btnA.setName("btnA");
		
		JLabel label_76 = new JLabel("");
		label_76.setBounds(258, 44, 0, 0);
		frameMeca.getContentPane().add(label_76);
		
		JLabel label_77 = new JLabel("");
		label_77.setBounds(263, 44, 0, 0);
		frameMeca.getContentPane().add(label_77);
		
		JLabel label_78 = new JLabel("");
		label_78.setBounds(268, 44, 0, 0);
		frameMeca.getContentPane().add(label_78);
		
		JButton btnS = new JButton("S");
		btnS.setBounds(375, 569, 55, 55);
		btnS.setEnabled(false);
		frameMeca.getContentPane().add(btnS);
		btnS.setName("btnS");
		
		JLabel label_79 = new JLabel("");
		label_79.setBounds(317, 44, 0, 0);
		frameMeca.getContentPane().add(label_79);
		
		JButton btnD = new JButton("D");
		btnD.setBounds(440, 569, 55, 55);
		btnD.setEnabled(false);
		frameMeca.getContentPane().add(btnD);
		btnD.setName("btnD");
		
		JLabel label_80 = new JLabel("");
		label_80.setBounds(366, 44, 0, 0);
		frameMeca.getContentPane().add(label_80);
		
		JButton btnF = new JButton("F");
		btnF.setBounds(505, 569, 55, 55);
		btnF.setEnabled(false);
		frameMeca.getContentPane().add(btnF);
		btnF.setName("btnF");
		
		JLabel label_81 = new JLabel("");
		label_81.setBounds(415, 44, 0, 0);
		frameMeca.getContentPane().add(label_81);
		
		JLabel label_82 = new JLabel("");
		label_82.setBounds(420, 44, 0, 0);
		frameMeca.getContentPane().add(label_82);
		
		JButton btnG = new JButton("G");
		btnG.setBounds(571, 569, 55, 55);
		btnG.setEnabled(false);
		frameMeca.getContentPane().add(btnG);
		btnG.setName("btnG");
		
		JLabel label_83 = new JLabel("");
		label_83.setBounds(469, 44, 0, 0);
		frameMeca.getContentPane().add(label_83);
		
		JButton btnH = new JButton("H");
		btnH.setBounds(636, 569, 55, 55);
		btnH.setEnabled(false);
		frameMeca.getContentPane().add(btnH);
		btnH.setName("btnH");
		
		JLabel label_84 = new JLabel("");
		label_84.setBounds(518, 44, 0, 0);
		frameMeca.getContentPane().add(label_84);
		
		JButton btnJ = new JButton("J");
		btnJ.setBounds(701, 569, 55, 55);
		btnJ.setEnabled(false);
		frameMeca.getContentPane().add(btnJ);
		btnJ.setName("btnJ");
		
		JLabel label_85 = new JLabel("");
		label_85.setBounds(565, 44, 0, 0);
		frameMeca.getContentPane().add(label_85);
		
		JButton btnK = new JButton("K");
		btnK.setBounds(767, 569, 55, 55);
		btnK.setEnabled(false);
		frameMeca.getContentPane().add(btnK);
		btnK.setName("btnK");
		
		JLabel label_86 = new JLabel("");
		label_86.setBounds(614, 44, 0, 0);
		frameMeca.getContentPane().add(label_86);
		
		JButton btnL = new JButton("L");
		btnL.setBounds(832, 569, 55, 55);
		btnL.setEnabled(false);
		frameMeca.getContentPane().add(btnL);
		btnL.setName("btnL");
		
		JLabel label_87 = new JLabel("");
		label_87.setBounds(661, 44, 0, 0);
		frameMeca.getContentPane().add(label_87);
		
		JButton btn� = new JButton("\u00D1");
		btn�.setBounds(897, 569, 55, 55);
		btn�.setEnabled(false);
		frameMeca.getContentPane().add(btn�);
		btn�.setName("btn�");
		
		JLabel label_88 = new JLabel("");
		label_88.setBounds(710, 44, 0, 0);
		frameMeca.getContentPane().add(label_88);
		
		JButton btnZ = new JButton("Z");
		btnZ.setBounds(344, 635, 55, 55);
		btnZ.setEnabled(false);
		frameMeca.getContentPane().add(btnZ);
		btnZ.setName("btnZ");
		
		JLabel label_89 = new JLabel("");
		label_89.setBounds(759, 44, 0, 0);
		frameMeca.getContentPane().add(label_89);
		
		JLabel label_90 = new JLabel("");
		label_90.setBounds(764, 44, 0, 0);
		frameMeca.getContentPane().add(label_90);
		
		JLabel label_91 = new JLabel("");
		label_91.setBounds(769, 44, 0, 0);
		frameMeca.getContentPane().add(label_91);
		
		JButton btnX = new JButton("X");
		btnX.setBounds(409, 635, 55, 55);
		btnX.setEnabled(false);
		frameMeca.getContentPane().add(btnX);
		btnX.setName("btnX");
		
		JLabel label_92 = new JLabel("");
		label_92.setBounds(818, 44, 0, 0);
		frameMeca.getContentPane().add(label_92);
		
		JButton btnC = new JButton("C");
		btnC.setBounds(475, 635, 55, 55);
		btnC.setEnabled(false);
		frameMeca.getContentPane().add(btnC);
		btnC.setName("btnC");
		
		JLabel label_93 = new JLabel("");
		label_93.setBounds(867, 44, 0, 0);
		frameMeca.getContentPane().add(label_93);
		
		JLabel label_94 = new JLabel("");
		label_94.setBounds(872, 44, 0, 0);
		frameMeca.getContentPane().add(label_94);
		
		JButton btnV = new JButton("V");
		btnV.setBounds(541, 635, 55, 55);
		btnV.setEnabled(false);
		frameMeca.getContentPane().add(btnV);
		btnV.setName("btnV");
		
		JLabel label_95 = new JLabel("");
		label_95.setBounds(921, 44, 0, 0);
		frameMeca.getContentPane().add(label_95);
		
		JButton btnB = new JButton("B");
		btnB.setBounds(606, 635, 55, 55);
		btnB.setEnabled(false);
		frameMeca.getContentPane().add(btnB);
		btnB.setName("btnB");
		
		JLabel label_96 = new JLabel("");
		label_96.setBounds(970, 44, 0, 0);
		frameMeca.getContentPane().add(label_96);
		
		JButton btnN = new JButton("N");
		btnN.setBounds(671, 635, 55, 55);
		btnN.setEnabled(false);
		frameMeca.getContentPane().add(btnN);
		btnN.setName("btnN");
		
		JLabel label_97 = new JLabel("");
		label_97.setBounds(1019, 44, 0, 0);
		frameMeca.getContentPane().add(label_97);
		
		JButton btnM = new JButton("M");
		btnM.setBounds(736, 635, 55, 55);
		btnM.setEnabled(false);
		frameMeca.getContentPane().add(btnM);
		btnM.setName("btnM");
		
		JLabel label_98 = new JLabel("");
		label_98.setBounds(1070, 44, 0, 0);
		frameMeca.getContentPane().add(label_98);
		
		JButton button_1 = new JButton(",");
		button_1.setBounds(801, 635, 55, 55);
		button_1.setEnabled(false);
		frameMeca.getContentPane().add(button_1);
		
		JLabel label_99 = new JLabel("");
		label_99.setBounds(415, 72, 0, 0);
		frameMeca.getContentPane().add(label_99);
		
		JButton button_2 = new JButton(".");
		button_2.setBounds(866, 635, 55, 55);
		button_2.setEnabled(false);
		frameMeca.getContentPane().add(button_2);
		
		JLabel label_100 = new JLabel("");
		label_100.setBounds(462, 72, 0, 0);
		frameMeca.getContentPane().add(label_100);
		
		JButton button_3 = new JButton("-");
		button_3.setBounds(929, 635, 55, 55);
		button_3.setEnabled(false);
		frameMeca.getContentPane().add(button_3);
		
		JLabel label_101 = new JLabel("");
		label_101.setBounds(509, 72, 0, 0);
		frameMeca.getContentPane().add(label_101);
		
		JLabel label_102 = new JLabel("");
		label_102.setBounds(514, 72, 0, 0);
		frameMeca.getContentPane().add(label_102);
		
		JLabel label_103 = new JLabel("");
		label_103.setBounds(519, 72, 0, 0);
		frameMeca.getContentPane().add(label_103);
		
		JLabel label_104 = new JLabel("");
		label_104.setBounds(524, 72, 0, 0);
		frameMeca.getContentPane().add(label_104);
		
		JLabel label_105 = new JLabel("");
		label_105.setBounds(529, 72, 0, 0);
		frameMeca.getContentPane().add(label_105);
		
		JLabel label_106 = new JLabel("");
		label_106.setBounds(534, 72, 0, 0);
		frameMeca.getContentPane().add(label_106);
		
		JLabel label_107 = new JLabel("");
		label_107.setBounds(539, 72, 0, 0);
		frameMeca.getContentPane().add(label_107);
		
		JLabel label_108 = new JLabel("");
		label_108.setBounds(544, 72, 0, 0);
		frameMeca.getContentPane().add(label_108);
		
		JLabel label_109 = new JLabel("");
		label_109.setBounds(549, 72, 0, 0);
		frameMeca.getContentPane().add(label_109);
		
		JLabel label_110 = new JLabel("");
		label_110.setBounds(554, 72, 0, 0);
		frameMeca.getContentPane().add(label_110);
		
		JLabel label_111 = new JLabel("");
		label_111.setBounds(559, 72, 0, 0);
		frameMeca.getContentPane().add(label_111);
		
		JLabel label_112 = new JLabel("");
		label_112.setBounds(564, 72, 0, 0);
		frameMeca.getContentPane().add(label_112);
		
		JLabel label_113 = new JLabel("");
		label_113.setBounds(569, 72, 0, 0);
		frameMeca.getContentPane().add(label_113);
		
		JLabel label_114 = new JLabel("");
		label_114.setBounds(574, 72, 0, 0);
		frameMeca.getContentPane().add(label_114);
		
		JLabel label_115 = new JLabel("");
		label_115.setBounds(579, 72, 0, 0);
		frameMeca.getContentPane().add(label_115);
		
		JLabel label_116 = new JLabel("");
		label_116.setBounds(584, 72, 0, 0);
		frameMeca.getContentPane().add(label_116);
		
		JLabel label_117 = new JLabel("");
		label_117.setBounds(589, 72, 0, 0);
		frameMeca.getContentPane().add(label_117);
		
		JLabel label_118 = new JLabel("");
		label_118.setBounds(594, 72, 0, 0);
		frameMeca.getContentPane().add(label_118);
		
		JLabel label_119 = new JLabel("");
		label_119.setBounds(599, 72, 0, 0);
		frameMeca.getContentPane().add(label_119);
		
		JLabel label_120 = new JLabel("");
		label_120.setBounds(604, 72, 0, 0);
		frameMeca.getContentPane().add(label_120);
		
		JLabel label_121 = new JLabel("");
		label_121.setBounds(609, 72, 0, 0);
		frameMeca.getContentPane().add(label_121);
		
		JLabel label_122 = new JLabel("");
		label_122.setBounds(614, 72, 0, 0);
		frameMeca.getContentPane().add(label_122);
		
		JLabel label_123 = new JLabel("");
		label_123.setBounds(619, 72, 0, 0);
		frameMeca.getContentPane().add(label_123);
		
		
		JButton Terminar = new JButton("Terminar");
		Terminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					int exit = JOptionPane.showConfirmDialog(frameMeca,
							"�Realmente desea salir?", "Salir",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
							
					if (exit == 0) {
							JFrame Ventana = new JFrame();
							Ventana.dispose();
							int save = JOptionPane.showConfirmDialog(frameMeca,
									"�Quieres guardar las puntuaciones actuales?", "Guardar",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE);
						
							if (save == 0) {
								LectorFicheros Escribir = new LectorFicheros();
								Escribir.EscribirFichero(nombre, "archivos\\Ranking"+nombre+".txt", count, error, aciertos);
								System.exit(0);
							}
							else if (save == 1){
								Ventana.setVisible(true);
								System.exit(0);
							}
					} else if (exit == 1) {
						System.exit(0);
					}
			}
		});
		
		Terminar.setBounds(929, 780, 116, 55);
		frameMeca.getContentPane().add(Terminar);
		frameMeca.getContentPane().setFocusable(true);
		
		acertado = new JTextArea();
		acertado.setText("Aciertos: ");
		acertado.setEnabled(false);
		acertado.setBounds(415, 5, 106, 27);
		frameMeca.getContentPane().add(acertado);
		
		text = new JTextArea();
		text.setEnabled(false);
		text.setBounds(250, 128, 671, 348);
		frameMeca.getContentPane().add(text);
		
		JButton btnSpace = new JButton("Space");
		btnSpace.setEnabled(false);
		btnSpace.setBounds(348, 708, 581, 49);
		frameMeca.getContentPane().add(btnSpace);
		btnSpace.setName("btnSpace");
		//frameMeca.requestFocus();
		
		
	//#EVENTOS
		
		primeralec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Texto ="Error";
				try {
					Texto = LectorFicheros.contenidoArchivo("archivos//Leccion1.txt");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String[] lecciones = Texto.split("###");
				text.setText(lecciones[0]);
				ArrayTexto = lecciones[0].toCharArray();
				IniciaTimer();
				primeralec.setEnabled(false);
				segundalec.setEnabled(false);
				frameMeca.getContentPane().requestFocus();
				
			}
		});
		segundalec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Texto ="Error";
				try {
					Texto = LectorFicheros.contenidoArchivo("archivos//Leccion1.txt");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String[] lecciones = Texto.split("###");
				text.setText(lecciones[1]);
				ArrayTexto = lecciones[1].toCharArray();
				IniciaTimer();
				primeralec.setEnabled(false);
				segundalec.setEnabled(false);
				frameMeca.getContentPane().requestFocus();
				
			}
		});
		frameMeca.getContentPane().addKeyListener( new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent letra) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void keyReleased(KeyEvent letra) {
				// TODO Auto-generated method stub
				
				buscarTecla(letra.getKeyChar()).setBackground(null);
			}
			
			@Override
			public void keyPressed(KeyEvent letra) {
				// TODO Auto-generated method stub
				
				contador(letra.getKeyChar());
			}
		});
		
	}
	public JButton buscarTecla(char letra) {
		JButton boton = null; 
		if(Character.isAlphabetic(letra) || letra == ' ') {
			for (int i = 0; i < frameMeca.getContentPane().getComponentCount() && boton == null; i++) {
				Component botones = frameMeca.getContentPane().getComponent(i);
				if (botones.getName() != null) {	
					if(letra == ' ') {
						if (botones.getName().equals("btnSpace")) {
							boton = (JButton) botones;
						}
					} else if ( botones.getName().equals("btn" + Character.toUpperCase(letra))) {
								boton = (JButton) botones;
					}
				}
			}
		} else {
			boton = new JButton("Error");
			boton.setEnabled(false);
			frameMeca.getContentPane().add(boton);
		}
		return boton;
	}
}
