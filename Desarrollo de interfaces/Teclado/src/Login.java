import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.awt.Toolkit;

public class Login {

	private static JFrame frmLogin;
	private JTextField user;
	private JPasswordField pass;
	private String usuario;
	

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}
	public JPasswordField getPass() {
		return pass;
	}

	public void setPass(JPasswordField pass) {
		this.pass = pass;
	}
	
	public static JFrame getFrmLogin() {
		return frmLogin;
	}

	public void setFrmLogin(JFrame frmLogin) {
		this.frmLogin = frmLogin;
	}
	
	public JTextField getUser() {
		return user;
	}

	public void setUser(JTextField user) {
		user = user;
	}

	public JPasswordField getPasswordField() {
		return pass;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.pass = passwordField;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
			frmLogin = new JFrame();
			frmLogin.setTitle("Mecano");
			frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
			frmLogin.setResizable(false);
			frmLogin.setBounds(100, 100, 654, 515);
			frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frmLogin.getContentPane().setLayout(null);
			
			JLabel lblUsuario = new JLabel("Usuario");
			lblUsuario.setForeground(new Color(255, 255, 255));
			lblUsuario.setFont(new Font("Times New Roman", Font.BOLD, 20));
			lblUsuario.setBounds(340, 289, 106, 64);
			frmLogin.getContentPane().add(lblUsuario);
			
			JLabel lblContrasea = new JLabel("Contrase\u00F1a");
			lblContrasea.setForeground(new Color(255, 255, 255));
			lblContrasea.setFont(new Font("Times New Roman", Font.BOLD, 20));
			lblContrasea.setBounds(340, 339, 130, 68);
			frmLogin.getContentPane().add(lblContrasea);
			
			user = new JTextField();
			user.setBounds(506, 314, 106, 20);
			frmLogin.getContentPane().add(user);
			user.setColumns(10);
			
			JButton btnNewButton = new JButton("Login");
			btnNewButton.setBounds(523, 434, 89, 23);
			frmLogin.getContentPane().add(btnNewButton);
			
			JLabel lblSobreNosotros = new JLabel("Sobre Nosotros...\r\n");
			lblSobreNosotros.setForeground(UIManager.getColor("textHighlight"));
			lblSobreNosotros.setFont(new Font("Times New Roman", Font.BOLD, 20));
			lblSobreNosotros.setBounds(468, 11, 144, 32);
			frmLogin.getContentPane().add(lblSobreNosotros);
			
			pass = new JPasswordField();
			pass.setEnabled(false);
			pass.setBounds(506, 366, 106, 20);
			frmLogin.getContentPane().add(pass);
			
			JLabel lblMecano = new JLabel("Mecano");
			lblMecano.setForeground(UIManager.getColor("textHighlight"));
			lblMecano.setFont(new Font("Magneto", Font.BOLD, 55));
			lblMecano.setBounds(10, 192, 283, 88);
			frmLogin.getContentPane().add(lblMecano);
			
			JLabel label = new JLabel("");
			label.setIcon(new ImageIcon("img\\mecanografia.jpg"));
			label.setBounds(0, 0, 657, 495);
			frmLogin.getContentPane().add(label);
			
			//EVENTOS
	
			user.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent arg0) {
					pass.setEnabled(true);
				}
			});
			
			lblSobreNosotros.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					Nosotros nosotros = new Nosotros();
					Nosotros.getFrame().setVisible(true);
				}
			});
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Login();
				}
			});
		}

	public void Login() {
		
		boolean log = false;
		String password = String.valueOf(pass.getPassword());
	    usuario = user.getText();
		
			try {
				LectorFicheros leer = new LectorFicheros();
				log = leer.find("archivos\\logs.txt", password, usuario);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (log) {
				Estadisticas estadistica = new Estadisticas(); 
				estadistica.getFrameEsta().setVisible(true);
				estadistica.setNombre(usuario);
				frmLogin.setVisible(false);
			} else {
				JOptionPane.showMessageDialog(null, "El login es Incorrecto, vuelva a intentarlo");
			}
		}
}
