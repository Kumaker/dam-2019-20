import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class Portada {
	
	Timer timer;
	int i = 0;
	
	private JFrame frmPortada;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Portada window = new Portada();
					window.frmPortada.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static boolean ComprobarFichero(String fichero) {
		boolean respuesta = false;
		
		try {
		File file = new File(fichero);
		if (file.exists()) {
			respuesta = file.exists();
		} else {
			JOptionPane.showMessageDialog(null ,"No se ha cargado bien el fichero: " + fichero);
			System.exit(0);
		}
		}catch (Exception e){
			e.getMessage();
		}
		return respuesta;
	}

	/**
	 * Create the application.
	 */
	public Portada() {
		initialize();
	}
	public JFrame getFrmPortada() {
		return frmPortada;
	}

	public void setFrmPortada(JFrame frmPortada) {
		this.frmPortada = frmPortada;
	}
	
	public int rellenaBarra() {
		
		int rellena = rellenaBarra();
		return rellena+1;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmPortada = new JFrame();
		frmPortada.setTitle("Mecano");
		frmPortada.setIconImage(Toolkit.getDefaultToolkit().getImage("img//minimize_78340.png"));
		frmPortada.setResizable(false);
		frmPortada.setBounds(100, 100, 654, 515);
		frmPortada.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPortada.getContentPane().setLayout(null);
		
		JProgressBar barraProgreso = new JProgressBar();
		barraProgreso.setValue(0);
		barraProgreso.setToolTipText("");
		barraProgreso.setBounds(211, 396, 246, 20);
		frmPortada.getContentPane().add(barraProgreso);
		
		JLabel lblMecano = new JLabel("Mecano");
		lblMecano.setForeground(UIManager.getColor("textHighlight"));
		lblMecano.setFont(new Font("Magneto", Font.BOLD, 55));
		lblMecano.setBounds(10, 192, 283, 88);
		frmPortada.getContentPane().add(lblMecano);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("img\\mecanografia.jpg"));
		label.setBounds(0, 0, 657, 495);
		frmPortada.getContentPane().add(label);
		
		// EVENTOS
		
		Timer timer = new Timer (25, new ActionListener ()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	i++;
		    	barraProgreso.setValue(i + 1);
			    	if( i == 100) {
			    		ComprobarFichero("archivos//Leccion1.txt");
			    		ComprobarFichero("archivos//RankingAndrea.txt");
			    		ComprobarFichero("archivos//RankingMiguel.txt");
			    		ComprobarFichero("archivos//Logs.txt");	
						Login login = new Login();
						login.getFrmLogin().setVisible(true);
						frmPortada.dispose();					    				    		
		    	}
		    }
		        // Aqu� el c�digo que queramos ejecutar
		});

		timer.start();
		}
	}
