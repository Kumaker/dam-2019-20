﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria
{
    public partial class Luigi : Form
    {
        public Luigi()
        {
            InitializeComponent();
            Pizzas.SelectedIndex = 0;
            Tamaños.SelectedIndex = 0;
            Masas.SelectedIndex = 0;
            Añadir.Enabled = true;
        }

        private void InicializaOpciones()
        {
            IngrePri.Enabled = true;
            IngreSec.Enabled = true;
            Masas.Enabled = true;
            Tamaños.Enabled = true;

            for (int i = 0; i < IngreSec.Items.Count; i++)
            {
                IngreSec.Enabled = true;
                Masas.Enabled = true;
                Masas.SelectedIndex = 0;
                Tamaños.SelectedIndex = 0;
            }

            if (Pizzas.SelectedIndex == 0)
            {
                IngrePri.Items.Clear();
                IngrePri.Items.AddRange(new object[] {
                    "Jamón York",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"});
                IngreSec.Items.Clear();
                IngreSec.Items.AddRange(new object[] {
                    "Cebolla Roja",
                    "Peperoni",
                    "Carne picada",
                    "Pollo",
                    "Pulled Pork",
                    "Salchichas",
                    "Jamon serrano",
                    "Bacon",
                    "Piña",
                    "Aceitunas",
                    "Aceite",
                    "Jamón York",
                    "Tomate",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"
                });
                IngreSec.Enabled = false;
                Masas.Enabled = false;
                Tamaños.Enabled = false;
            }
            if (Pizzas.SelectedIndex == 1)
            {
                IngrePri.Items.Clear();
                IngrePri.Items.AddRange(new object[] {
                    "Jamón York",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"});
                IngreSec.Items.Clear();
                IngreSec.Items.AddRange(new object[] {
                    "Cebolla Roja",
                    "Peperoni",
                    "Carne picada",
                    "Pollo",
                    "Pulled Pork",
                    "Salchichas",
                    "Jamon serrano",
                    "Bacon",
                    "Piña",
                    "Aceitunas",
                    "Aceite",
                    "Jamón York",
                    "Tomate",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"
                });

                IngreSec.Enabled = true;
                IngrePri.Enabled = false;
                IngrePri.SetItemChecked(0, true);
                IngrePri.SetItemChecked(1, true);
                Tamaños.SelectedIndex = 0;
                Masas.SelectedIndex = 0;
            }
            if (Pizzas.SelectedIndex == 2)
            {
                IngrePri.Items.Clear();
                IngrePri.Items.AddRange(new object[] {
                    "Jamón York",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"});
                IngreSec.Items.Clear();
                IngreSec.Items.AddRange(new object[] {
                   "Champiñones",
                   "Piña",
                   "Cebolla Roja",
                   "Tomate",
                   "Aceitunas",
                   "Aceite"
                });
                IngrePri.SetItemChecked(2, true);
                IngrePri.SetItemChecked(3, true);
                IngrePri.Enabled = false;
                Tamaños.SelectedIndex = 1;
                Tamaños.Enabled = false;
            }
            if (Pizzas.SelectedIndex == 3)
            {
                IngrePri.Items.Clear();
                IngrePri.Items.AddRange(new object[] {
                    "Jamón York",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"});
                IngreSec.Items.Clear();
                IngreSec.Items.AddRange(new object[] {
                   "Cebolla Roja",
                    "Peperoni",
                    "Carne picada",
                    "Pollo",
                    "Pulled Pork",
                    "Salchichas",
                    "Jamon serrano",
                    "Bacon",
                    "Piña",
                    "Aceitunas",
                    "Aceite",
                    "Jamón York",
                    "Tomate",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"
                });

                Tamaños.SelectedIndex = 0;
                Masas.SelectedIndex = 0;

                IngrePri.SetItemChecked(4, true);
                IngrePri.SetItemChecked(5, true);
                IngrePri.Enabled = false;
            }
            if (Pizzas.SelectedIndex == 4)
            {
                IngrePri.Items.Clear();
                IngrePri.Items.AddRange(new object[] {
                    "Jamón York",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera",
                    "Cabrales",
                    "Manchego",
                    "Queso de burgos",
                    "Cheddar"});
                IngreSec.Items.Clear();
                IngreSec.Items.AddRange(new object[] {
                   "Cebolla Roja",
                    "Peperoni",
                    "Carne picada",
                    "Pollo",
                    "Pulled Pork",
                    "Salchichas",
                    "Jamon serrano",
                    "Bacon",
                    "Piña",
                    "Aceitunas",
                    "Aceite",
                    "Jamón York",
                    "Tomate",
                    "Champiñón",
                    "Calabacín",
                    "Aceitunas",
                    "Tabasco",
                    "Ternera"
                });
                IngreSec.Enabled = false;
                IngrePri.Enabled = false;

                Masas.Enabled = false;
                Tamaños.Enabled = true;
                Tamaños.SelectedIndex = 0;
                Masas.SelectedIndex = 1;

                IngrePri.SetItemChecked(6, true);
                IngrePri.SetItemChecked(7, true);
                IngrePri.SetItemChecked(8, true);
                IngrePri.SetItemChecked(9, true);
            }
        }

        public void CompruebaSeleccionComponentes()
        {
            bool encontrado = false;

            for (int i = 0; (i < IngreSec.Items.Count) && (encontrado == false); i++)
            {
                if (IngreSec.GetItemChecked(i) || (IngreSec.CheckedItems.Count == 0))
                {
                    if ((IngreSec.CheckedItems.Count >= 2) && (IngreSec.CheckedItems.Count <= 4))
                    {
                        if ((Pizzas.SelectedIndex > 0) && (Masas.SelectedIndex > 0) && (Tamaños.SelectedIndex > 0))
                        {
                            MostrarResumen();
                            encontrado = true;
                        }
                    }
                    else if ((IngreSec.CheckedItems.Count == 0))
                    {
                        if ((Pizzas.SelectedIndex > 0) && (Masas.SelectedIndex > 0) && (Tamaños.SelectedIndex > 0))
                        {
                            MostrarResumen();
                            encontrado = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Como mínimo 2 y máximo 4 ingredientes extra", "Ingredientes Extra", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        encontrado = true;
                    }
                }
            }
            if (encontrado == false)
            {
                MessageBox.Show("Selecciona el tipo de pizza que quieres", "Faltan campos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void MostrarResumen()
        {
            string[] secundarios = new string[IngreSec.Items.Count];
            string[] principales = new string[IngrePri.Items.Count];

            Resumen.Text = "\n PEDIDO SOLICITADO: \n\t - Pizza: \n\t · " +
            Pizzas.SelectedItem + "\n\n\t - Ingredientes principales: \n\t";

            for (int i = 0; i < IngrePri.Items.Count; i++)
            {
                if (IngrePri.GetItemChecked(i))
                {
                    principales[i] = IngrePri.Items[i].ToString();
                    Resumen.Text += " · " + principales[i] + "\n\t";
                }
            }

            for (int i = 0; i < IngreSec.Items.Count; i++)
            {
                if (IngreSec.GetItemChecked(i))
                {
                    secundarios[i] = IngreSec.Items[i].ToString();
                    Resumen.Text += "Ingrediente extra\n\t\t" + secundarios[i] + "\n\t";
                }
            }
            Resumen.Text += "\n\t - Tamaño: \n\t · " + Tamaños.SelectedItem + "\n\n\t - Masa: \n\t · " + Masas.SelectedItem + "\n\n";
        }

        private void Pizzas_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            InicializaOpciones();
            /*if ((Pizzas.SelectedIndex != 0) && (Masas.SelectedIndex != 0) && (Tamaños.SelectedIndex != 0))
            {
                Añadir.Enabled = true;
            }*/
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            string mensage = "¿Realmente quieres salir?";
            string titulo = "Salir del programa";
            MessageBoxButtons opciones = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(mensage, titulo, opciones,
            MessageBoxIcon.Question);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Importe_Click_1(object sender, EventArgs e)
        {
                double total = 0;
                double Pizza = 0;
                double Masa = 0;
                double Tamaño = 0;
                double Ingredientes = 0;

                //PIZZAS
                if (Pizzas.SelectedIndex == 1)
                {
                    Pizza = 3;
                }
                if (Pizzas.SelectedIndex == 2)
                {
                    Pizza = 2.50;
                }
                if (Pizzas.SelectedIndex == 3)
                {
                    Pizza = 3.5;
                }
                if (Pizzas.SelectedIndex == 4)
                {
                    Pizza = 3;
                }

                //TAMAÑO
                if (Tamaños.SelectedIndex == 1)
                {
                    Tamaño = 10;
                }
                if (Tamaños.SelectedIndex == 2)
                {
                    Tamaño = 12.50;
                }
                if (Tamaños.SelectedIndex == 3)
                {
                    Tamaño = 20;
                }

                //MASA
                if (Masas.SelectedIndex == 1)
                {
                    Masa = 2;
                }
                if (Masas.SelectedIndex == 2)
                {
                    Masa = 2.5;
                }
                if (Masas.SelectedIndex == 3)
                {
                    Masa = 2;
                }
                if (Masas.SelectedIndex == 4)
                {
                    Masa = 4;
                }

                //INGREDIENTES
                Ingredientes = IngreSec.CheckedItems.Count;
                total = Pizza + Ingredientes + Tamaño + Masa;

                MessageBox.Show("El importe de su pedido es: " + total + "€", "Precio de la compra", MessageBoxButtons.OK, MessageBoxIcon.Question);


        }

        private void Añadir_Click(object sender, EventArgs e)
        {
            CompruebaSeleccionComponentes();
            Importe.Enabled = true;
        }
    }
}
