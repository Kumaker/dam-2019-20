﻿using System;

namespace Pizzeria
{
    partial class Luigi
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Luigi));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.NombreRestaurante = new System.Windows.Forms.Label();
            this.Pizzas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.IngrePri = new System.Windows.Forms.CheckedListBox();
            this.Añadir = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Tamaños = new System.Windows.Forms.ComboBox();
            this.Masas = new System.Windows.Forms.ComboBox();
            this.IngreSec = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Resumen = new System.Windows.Forms.RichTextBox();
            this.Importe = new System.Windows.Forms.Button();
            this.Salir = new System.Windows.Forms.Button();
            this.Precios = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-2, 315);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 226);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // NombreRestaurante
            // 
            this.NombreRestaurante.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NombreRestaurante.BackColor = System.Drawing.Color.Transparent;
            this.NombreRestaurante.Font = new System.Drawing.Font("Segoe Script", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreRestaurante.Location = new System.Drawing.Point(387, -22);
            this.NombreRestaurante.Name = "NombreRestaurante";
            this.NombreRestaurante.Size = new System.Drawing.Size(446, 85);
            this.NombreRestaurante.TabIndex = 2;
            this.NombreRestaurante.Text = "Benvenuto";
            // 
            // Pizzas
            // 
            this.Pizzas.FormattingEnabled = true;
            this.Pizzas.Items.AddRange(new object[] {
            "Selecciona Pizza",
            "NewYork",
            "Vegetariana",
            "Barbacoa Picante",
            "4 Quesos"});
            this.Pizzas.Location = new System.Drawing.Point(317, 110);
            this.Pizzas.Name = "Pizzas";
            this.Pizzas.Size = new System.Drawing.Size(126, 21);
            this.Pizzas.TabIndex = 3;
            this.Pizzas.Text = "Selecciona una pizza";
            this.Pizzas.SelectedIndexChanged += new System.EventHandler(this.Pizzas_SelectedIndexChanged_1);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label1.Location = new System.Drawing.Point(314, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Seleccione su pizza";
            // 
            // IngrePri
            // 
            this.IngrePri.FormattingEnabled = true;
            this.IngrePri.Items.AddRange(new object[] {
            "Jamón York",
            "Champiñón",
            "Calabacín",
            "Aceitunas",
            "Tabasco",
            "Ternera",
            "Cabrales",
            "Manchego",
            "Queso de burgos",
            "Cheddar"});
            this.IngrePri.Location = new System.Drawing.Point(361, 222);
            this.IngrePri.Name = "IngrePri";
            this.IngrePri.Size = new System.Drawing.Size(107, 154);
            this.IngrePri.TabIndex = 5;
            // 
            // Añadir
            // 
            this.Añadir.Enabled = false;
            this.Añadir.Location = new System.Drawing.Point(696, 385);
            this.Añadir.Name = "Añadir";
            this.Añadir.Size = new System.Drawing.Size(135, 52);
            this.Añadir.TabIndex = 8;
            this.Añadir.Text = "Añadir pedido";
            this.Añadir.UseVisualStyleBackColor = true;
            this.Añadir.Click += new System.EventHandler(this.Añadir_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label2.Location = new System.Drawing.Point(669, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Seleccione su Tamaño";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label3.Location = new System.Drawing.Point(669, 282);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(167, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Seleccione su masa";
            // 
            // Tamaños
            // 
            this.Tamaños.FormattingEnabled = true;
            this.Tamaños.Items.AddRange(new object[] {
            "Selecciona Tamaño",
            "Pequeña",
            "Mediana",
            "Familiar"});
            this.Tamaños.Location = new System.Drawing.Point(696, 222);
            this.Tamaños.Name = "Tamaños";
            this.Tamaños.Size = new System.Drawing.Size(126, 21);
            this.Tamaños.TabIndex = 15;
            this.Tamaños.Text = "Selecciona tamaño";
            // 
            // Masas
            // 
            this.Masas.FormattingEnabled = true;
            this.Masas.Items.AddRange(new object[] {
            "Selecciona Masa",
            "Fina",
            "Pan",
            "Tradicional",
            "Bordes Rellenos"});
            this.Masas.Location = new System.Drawing.Point(696, 315);
            this.Masas.Name = "Masas";
            this.Masas.Size = new System.Drawing.Size(126, 21);
            this.Masas.TabIndex = 16;
            this.Masas.Text = "Selecciona masa";
            // 
            // IngreSec
            // 
            this.IngreSec.FormattingEnabled = true;
            this.IngreSec.Items.AddRange(new object[] {
            "Cebolla Roja",
            "Peperoni",
            "Carne picada",
            "Pollo",
            "Pulled Pork",
            "Salchichas",
            "Jamon serrano",
            "Bacon",
            "Piña",
            "Aceitunas",
            "Aceite",
            "Jamón York",
            "Tomate",
            "Champiñón",
            "Calabacín",
            "Aceitunas",
            "Tabasco",
            "Ternera",
            "Cabrales",
            "Manchego",
            "Queso de burgos",
            "Cheddar"});
            this.IngreSec.Location = new System.Drawing.Point(543, 142);
            this.IngreSec.Name = "IngreSec";
            this.IngreSec.Size = new System.Drawing.Size(107, 334);
            this.IngreSec.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label4.Location = new System.Drawing.Point(314, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 18);
            this.label4.TabIndex = 18;
            this.label4.Text = "Ingredientes Principales";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label5.Location = new System.Drawing.Point(501, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 18);
            this.label5.TabIndex = 19;
            this.label5.Text = "Ingredientes Secundarios";
            // 
            // Resumen
            // 
            this.Resumen.Location = new System.Drawing.Point(861, 121);
            this.Resumen.Name = "Resumen";
            this.Resumen.Size = new System.Drawing.Size(257, 352);
            this.Resumen.TabIndex = 20;
            this.Resumen.Text = "";
            // 
            // Importe
            // 
            this.Importe.Enabled = false;
            this.Importe.Location = new System.Drawing.Point(923, 63);
            this.Importe.Name = "Importe";
            this.Importe.Size = new System.Drawing.Size(135, 52);
            this.Importe.TabIndex = 21;
            this.Importe.Text = "Importe";
            this.Importe.UseVisualStyleBackColor = true;
            this.Importe.Click += new System.EventHandler(this.Importe_Click_1);
            // 
            // Salir
            // 
            this.Salir.Location = new System.Drawing.Point(254, 464);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(135, 52);
            this.Salir.TabIndex = 22;
            this.Salir.Text = "Salir";
            this.Salir.UseVisualStyleBackColor = true;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // Precios
            // 
            this.Precios.AutoSize = true;
            this.Precios.Location = new System.Drawing.Point(12, 66);
            this.Precios.Name = "Precios";
            this.Precios.Size = new System.Drawing.Size(170, 234);
            this.Precios.TabIndex = 23;
            this.Precios.Text = resources.GetString("Precios.Text");
            // 
            // Luigi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1163, 539);
            this.Controls.Add(this.Precios);
            this.Controls.Add(this.Salir);
            this.Controls.Add(this.Importe);
            this.Controls.Add(this.Resumen);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.IngreSec);
            this.Controls.Add(this.Masas);
            this.Controls.Add(this.Tamaños);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Añadir);
            this.Controls.Add(this.IngrePri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pizzas);
            this.Controls.Add(this.NombreRestaurante);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Luigi";
            this.Text = "Ristorante Luigi";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label NombreRestaurante;
        private System.Windows.Forms.ComboBox Pizzas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox IngrePri;
        private System.Windows.Forms.Button Añadir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Tamaños;
        private System.Windows.Forms.ComboBox Masas;
        private System.Windows.Forms.CheckedListBox IngreSec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox Resumen;
        private System.Windows.Forms.Button Importe;
        private System.Windows.Forms.Button Salir;
        private System.Windows.Forms.Label Precios;
    }
}

