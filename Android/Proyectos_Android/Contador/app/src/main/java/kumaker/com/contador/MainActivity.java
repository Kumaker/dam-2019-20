package kumaker.com.contador;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    public int cont;

    TextView textoResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textoResultado=(TextView)findViewById(R.id.contadorTexto);
        cont = 0;

        EventoTeclado teclado=new EventoTeclado();

        EditText n_reseteo=(EditText)findViewById(R.id.n_reseteo);

        n_reseteo.setOnEditorActionListener(teclado);
    }

    class EventoTeclado implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            if(actionId== EditorInfo.IME_ACTION_DONE) {

                reseteaContador(null);
            }

            return false;
        }
    }

    public void incrementaContador(View vista){

        cont++;

        textoResultado.setText("" + cont);
    }

    public void decrementaContador(View vista){

        cont--;

        if (cont <0){
            CheckBox negativos=(CheckBox)findViewById(R.id.negativos);

            if(!negativos.isChecked()){
                cont = 0;
            }
        }

        textoResultado.setText("" + cont);

    }

    public void reseteaContador(View vista){

        EditText numero_reset=(EditText)findViewById(R.id.n_reseteo);

        try {
            cont = Integer.parseInt(numero_reset.getText().toString());
        }catch (Exception e){
            cont=0;
        }
        numero_reset.setText("");

        textoResultado.setText("" + cont);
    }
}
