package kumaker.com.crud_basededatos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {

    Button botonInsertar, botonActualizar, botonBorrar, botonBuscar;

    EditText textoId, textoNombre, textoApellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonInsertar=(Button)findViewById(R.id.insertar);
        botonActualizar=(Button)findViewById(R.id.actualizar);
        botonBorrar=(Button)findViewById(R.id.borrar);
        botonBuscar=(Button)findViewById(R.id.buscar);

        textoId=(EditText)findViewById(R.id.id);
        textoNombre=(EditText)findViewById(R.id.nombre);
        textoApellido=(EditText)findViewById(R.id.apellido);

        final BBDD_Helper helper = new BBDD_Helper(this);

        botonInsertar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Gets the data repository in write mode
                SQLiteDatabase db = helper.getWritableDatabase();

                // Create a new map of values, where column names are the keys
                ContentValues values = new ContentValues();
                values.put(Estructura_BBDD.NOMBRE_COLUMNA1, textoId.getText().toString());
                values.put(Estructura_BBDD.NOMBRE_COLUMNA2, textoNombre.getText().toString());
                values.put(Estructura_BBDD.NOMBRE_COLUMNA3, textoApellido.getText().toString());

                // Insert the new row, returning the primary key value of the new row
                long newRowId = db.insert(Estructura_BBDD.TABLE_NAME, null, values);
                Toast.makeText(getApplicationContext(), "Se guardo el registro con clave: " + newRowId, Toast.LENGTH_LONG).show();

            }
        });

        botonActualizar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        botonBorrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                SQLiteDatabase db = helper.getReadableDatabase();

                // Define a projection that specifies which columns from the database
                // you will actually use after this query.
                String[] projection = {
                        //BaseColumns._ID,
                        Estructura_BBDD.NOMBRE_COLUMNA2,
                        Estructura_BBDD.NOMBRE_COLUMNA3
                };

                // Filter results WHERE "title" = 'My Title'
                String selection = Estructura_BBDD.NOMBRE_COLUMNA1 + " = ?";
                String[] selectionArgs = { textoId.getText().toString() };

                // How you want the results sorted in the resulting Cursor
                /*String sortOrder =
                        FeedEntry.COLUMN_NAME_SUBTITLE + " DESC";*/

                Cursor cursor = db.query(
                        Estructura_BBDD.TABLE_NAME,   // The table to query
                        projection,             // The array of columns to return (pass null to get all)
                        selection,              // The columns for the WHERE clause
                        selectionArgs,          // The values for the WHERE clause
                        null,          // don't group the rows
                        null,           // don't filter by row groups
                        null            // The sort order
                );

                cursor.moveToFirst();

                textoNombre.setText(cursor.getString(0));

                textoApellido.setText(cursor.getString(1));



            }
        });

    }
}
