#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C"{
#endif

int sum (int op1, int op2);
int res (int op1, int op2);
int mul (int op1, int op2);
int div (int op1, int op2);

#ifdef __cplusplus
}
#endif
#endif
