#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

  int spawn (char* program, char** arg_list){
    pid_t child_pid;
    child_pid = fork ();
      if (child_pid != 0)
        /*Este es el padre*/
         return child_pid;
      else {
          /*Este es el hijo*/
        execvp(program, arg_list);
        fprintf (stderr, "Un error en execvp\n");
        abort();
      }
}
  int main (int argc, char *argv[]){
      int child_status;
      char* arg_list[] = {"./sumar","5", "4", NULL};
      spawn("./sumar", arg_list);
      wait (&child_status);
         if (WIFEXITED (child_status))
             printf ("La suma creada por el hijo es %d\n",
                     WEXITSTATUS (child_status));
         else {
             printf("Ha habido un error");
          }

    return EXIT_SUCCESS;
}
