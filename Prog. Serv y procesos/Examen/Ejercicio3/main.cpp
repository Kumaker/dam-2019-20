#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX_LADRILLOS 50
#define DELAY 1

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int ladrillosCogidos = 0;
int ladrillosCadaVez = 0;
int ladrillosAlmacenados = MAX_LADRILLOS;
int ladrillosFabrica = 0;

sem_t sem_id;

void LadrillosLlevados(){

  int total = 0;

  total = (rand () %2) + 1;

  if(total == 1)
      ladrillosCadaVez = 1;
  else{
    if(ladrillosFabrica == MAX_LADRILLOS - 1)
      ladrillosCadaVez = 1;
    else
      ladrillosCadaVez = 2;
  }
}

void* ObreroMariano (void* args) {

    while(ladrillosCogidos < MAX_LADRILLOS){
      sem_wait (&sem_id);
      pthread_mutex_lock (&mutex);

      if(ladrillosFabrica < MAX_LADRILLOS){
        LadrillosLlevados();
        printf("Mariano Lleva %i ladrillos en mano\n", ladrillosCadaVez);
        ladrillosFabrica += ladrillosCadaVez;
        ladrillosCogidos += ladrillosCadaVez;
        printf("ladrillos en fabrica %i\n", ladrillosFabrica);
      }

      pthread_mutex_unlock (&mutex);
      sem_post (&sem_id);

      if(ladrillosFabrica < MAX_LADRILLOS)
          printf("Mariano se va a dormir, le toca a Benito. \n\n");
      else
          printf("Mariano llevo el ultimo ladrillo. \n\n");
      sleep(DELAY);
    }

    return NULL;
  }

void* ObreroBenito (void* args) {

    while(ladrillosCogidos < MAX_LADRILLOS){
      sem_wait (&sem_id);
      pthread_mutex_lock (&mutex);

      if(ladrillosFabrica < MAX_LADRILLOS){
        LadrillosLlevados();
        printf("Benito Lleva %i ladrillos en mano\n", ladrillosCadaVez);
        ladrillosFabrica += ladrillosCadaVez;
        ladrillosCogidos += ladrillosCadaVez;
        printf("ladrillas en fabrica %i\n", ladrillosFabrica);
      }

      pthread_mutex_unlock (&mutex);
      sem_post (&sem_id);

      if(ladrillosFabrica < MAX_LADRILLOS)
          printf("Benito se va a dormir, le toca a Benito. \n\n");
      else
          printf("Benito llevo el ultimo ladrillo. \n\n");
      sleep(DELAY);
    }

    return NULL;
  }


int main (int argc, char* argv[]){

    pthread_t Benito;
      pthread_t Mariano;

    srand (time (NULL));

    sem_init (&sem_id, 0, 1);

    pthread_create (&Benito, NULL, &ObreroBenito, &sem_id);
    pthread_create (&Mariano, NULL, &ObreroMariano, &sem_id);

    pthread_join (Benito, NULL);
      printf ("Benito acabo.\n");
    pthread_join (Mariano, NULL);
      printf ("Mariano acabo.\n");

      sem_destroy (&sem_id);

      return EXIT_SUCCESS;
}
