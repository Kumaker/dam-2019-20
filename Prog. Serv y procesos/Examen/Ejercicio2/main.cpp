#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#define DELAY 2
#define NUM_SIGNALS 3
#define SON "./hijo"

int spawn (){
    pid_t child_pid;
    child_pid = fork ();

    if(child_pid !=0){
      return child_pid;
    } else {
        execvp(SON, NULL);
        fprintf (stderr, "Un error ocurrio en execvp\n");
        abort ();
    }
}

int main (){
    pid_t pid_child;
    int child_status;

    pid_child = spawn ();

    for(int i = 0; i < NUM_SIGNALS; i++) {

      sleep(DELAY);
      kill(pid_child, SIGUSR1);
      printf("Enviadas: %i señales\n", i+1);
    }

    wait(&child_status);

    if(WIFEXITED (child_status))
        printf("El proceso hijo salio bien \n");
    else
        printf ("El proceso hijo no ha salido bien\n");

    return EXIT_SUCCESS;
}
