#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define DELAY 2 
#define NUM_SIGNALS 3

sig_atomic_t sigusr1_count = 0;

void handler (int signal_nombre){
  ++sigusr1_count;
}

int main(){
  struct sigaction sa;

  memset (&sa, 0, sizeof(sa));
  sa.sa_handler = &handler;
  sigaction (SIGUSR1, &sa, NULL);

  while(sigusr1_count < NUM_SIGNALS){
    sleep(DELAY);
    printf("Hola, %i señales recibidas\n", sigusr1_count);
  }
  return  EXIT_SUCCESS;
}

